# -*- coding: utf-8 -*-
"""Create turbulence boxes and hawc2 files for each constraint file in a given directory.

Default functions are power profile, iec_sig and kaimal_spectrum. They require:
    - power profile: u_ref, z_ref and alpha
    - iec_sig: turb_class
    - kaimal_spectrum: u_ref

Use default IEC coherence.
"""
import os
import numpy as np
import pandas as pd
from pyconturb import gen_spat_grid, gen_turb, TimeConstraint
from pyconturb.coherence import generate_coherence_file
from pyconturb.io import df_to_h2turb
from pyconturb._utils import get_freq
from _pct22utils import fit_power_law, make_turb_htc
from _pct22vals import (dtype, kw_dtu10mw, kw_nrel5mw, names, nwid, nht, wsps, con_dir,
                        coh_dir, seed_ext)



seed_unc = 1001
ds = [kw_dtu10mw, kw_nrel5mw]
overwrite = False

for i, (d, name) in enumerate(zip(ds, names)):
    for wsp in wsps:
        print(f'Generating coherence for {name}, {wsp} m/s...')
        
        fname = name + f'_wsp{wsp}_s{seed_unc}'  # dtu_10mw_wsp11.4_s1001
        
        # constraint file (needed for u component)
        con_file = fname + f'_{seed_ext}_ext.con'
        con_path = con_dir + con_file
        con_tc = TimeConstraint(pd.read_csv(con_path, index_col=0))  # load data from csv directly into tc
        con_tc.index = con_tc.index.map(lambda x: float(x) if (x not in 'kxyz') else x)  # index cleaning
        
        # create the spat_df
        y = np.linspace(-d['tb_wid']/2, d['tb_wid']/2, nwid)
        z = np.linspace(-d['tb_ht']/2 + d['z_hub'], d['tb_ht']/2 + d['z_hub'], nht)
        
        # assemble the kwargs for calcaulte_coh_mat [u_ref and l_c, that's it]
        kwargs = {'u_ref': wsp, 'l_c': 340.2,
                  'nf_chunk': 20, 'verbose': True, 'dtype': dtype}
        
        # frequency array for v, w components
        freq = get_freq(T=con_tc.get_T(), nt=con_tc.get_nt())[1]
        
        for icomp, comp in enumerate('uvw'):

            spat_df = gen_spat_grid(y, z, comps=[icomp])
        
            # coherence file name/path
            coh_file = fname + f'{comp}_coh.h5'  # dtu_10mw_wsp11.4_s1001u_coh.h5
            coh_path = coh_dir + coh_file
            
            if os.path.isfile(coh_path) and not overwrite:
                print(f'  jk, component {"uvw"[icomp] exists. Skipping.')
                break
            
            if icomp:  # v, w
                kw = {**kwargs, **{'freq': freq}}
            else:  # u
                kw = {**kwargs, **{'con_tc': con_tc}}
            
            generate_coherence_file(spat_df, coh_path, **kw)
