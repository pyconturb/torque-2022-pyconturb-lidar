# -*- coding: utf-8 -*-
"""Create table of standard deviations
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from _pct22vals import kw_dtu10mw, kw_nrel5mw, wsps, names

ds = [kw_dtu10mw, kw_nrel5mw]
stats_df = pd.read_csv('torque2022_stats.csv', index_col=0)

m = 4  # only tower-base moments, yo
errkeys = [('idx_rawp', 'mae'),
           ('idx_om', 'mae'),
           ('idx_pit', 'mae'),
           ('idx_tbfa', 'mae'),
           ('idx_tbfa', 'del'),
           ]

for key, errtype in errkeys:

    print('\n', key.upper(), errtype, '\n')
    norm = 1e-3 if 'tb' in key else 1
    
    for j, wsp in enumerate(wsps):
        
        s = f' & {wsp} m/s '
        
        for i, (d, name) in enumerate(zip(ds, names)):
            
            orig_df = stats_df[(stats_df.channel == key) &
                              (stats_df.wsp == wsp) &
                              (stats_df.name == name) &
                              (stats_df.type == 'orig')]
            unc_df = stats_df[(stats_df.channel == key) &
                                  (stats_df.wsp == wsp) &
                                  (stats_df.name == name) &
                                  (stats_df.type == 'unc')]
            con_df = stats_df[(stats_df.channel == key) &
                                  (stats_df.wsp == wsp) &
                                  (stats_df.name == name) &
                                  (stats_df.type == 'con')]
            
            if errtype == 'mae':
                unc_val = np.mean(unc_df['mae'] * norm)
                con_val = np.mean(con_df['mae'] * norm)
                
                err = (con_val - unc_val) / unc_val
                
                s += ' & {0:.2f} & {1:.2f} ({2:.0%})'.format(unc_val, con_val, err)
                
            else:
                orig_del = float(orig_df['del']) * norm
                unc_del = np.mean((unc_df['del'] * norm)**m) ** (1/m)
                con_del = np.mean((con_df['del'] * norm)**m) ** (1/m)
                
                unc_nefl = (unc_del - orig_del) / orig_del
                con_nefl = (con_del - orig_del) / orig_del
                
                err = (abs(con_nefl) - abs(unc_nefl))
                
                s += ' & {0:.1%} & {1:.1%} ({2:.0%})'.format(unc_nefl, con_nefl, err)
        
        s += ' \\\\'
        print(s)
        
        
        


# for i, (d, name) in enumerate(zip(ds, names)):
    
#     print('\n', name, '\n')
            
    
#     print(' & '.join(['Theo ', ' Orig      ', '  Uncon   ', '  Con  ']) + '\\\\')
#     for j, wsp in enumerate(wsps):
        
#         sigu_origs = np.empty(4)
#         sigu_uncs = np.empty(4)
#         sigu_cons = np.empty(4)
#         for ibeam in range(1, 5):
#             key = f'idc_freewind{ibeam}'
    
#             sigu_theo = 0.16 * (0.75 * wsp + 5.6)
            
#             orig_df = stats_df[(stats_df.channel == key) &
#                               (stats_df.wsp == wsp) &
#                               (stats_df.name == name) &
#                               (stats_df.type == 'orig')]
#             unc_df = stats_df[(stats_df.channel == key) &
#                                   (stats_df.wsp == wsp) &
#                                   (stats_df.name == name) &
#                                   (stats_df.type == 'unc')]
#             con_df = stats_df[(stats_df.channel == key) &
#                                   (stats_df.wsp == wsp) &
#                                   (stats_df.name == name) &
#                                   (stats_df.type == 'con')]
            
#             sigu_origs[ibeam-1] = float(orig_df['std'].values)
#             sigu_uncs[ibeam-1] = np.sqrt(np.mean(unc_df['std'].values**2))
#             sigu_cons[ibeam-1] = np.sqrt(np.mean(con_df['std'].values**2))
        
#         sigu_orig = np.sqrt(np.mean(sigu_origs**2))
#         sigu_unc = np.sqrt(np.mean(sigu_uncs**2))
#         sigu_con = np.sqrt(np.mean(sigu_cons**2))
        
#         err_unc = float((sigu_unc - sigu_orig) / sigu_orig)
#         err_con = float((sigu_con - sigu_orig) / sigu_orig)
        
        
#         s = ['{0:.2f}'.format(sigu_theo),
#             '{0:.2f}'.format(sigu_orig),
#             '{0:.2f} ({1:.0%})'.format(sigu_unc, err_unc),
#             '{0:.2f} ({1:.0%})'.format(sigu_con, err_con),
#             ]
#         print('  &  '.join(s), ' \\\\')

