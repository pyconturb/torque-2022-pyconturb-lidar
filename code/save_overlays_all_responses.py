# -*- coding: utf-8 -*-
"""Pretty visualization of the rotor-average wind speeds with and without constraints.
"""
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import numpy as np
from _pct22utils import load_hawc2_res
from _pct22vals import kw_dtu10mw, kw_nrel5mw, lidar_upwind, seed_ext, seed_orig, seeds_con


name, d = 'dtu_10mw', kw_dtu10mw
# name, d = 'nrel_5mw', kw_nrel5mw
wsp = 11.4

# load all needed data
fname_orig =  f'{name}_wsp{wsp}_s{seed_orig}'
ds_orig = load_hawc2_res(name, fname_orig)
ds_unc = np.empty((ds_orig.shape[0], ds_orig.shape[1], 6))  # nt x nchan x 6
for iunc, seed_unc in enumerate(range(seed_orig, seed_orig+6)):
    fname_sim =  f'{name}_wsp{wsp}_s{seed_unc}'
    ds_unc[:, :, iunc] = load_hawc2_res(name, fname_sim)
ds_con = np.empty((ds_orig.shape[0], ds_orig.shape[1], 6))
for icon, seed_con in enumerate(seeds_con):
    fname_con =  f'{name}_wsp{wsp}_s{seed_orig}_{seed_ext}_ext_cs{seed_con}'
    ds_con[:, :, icon] = load_hawc2_res(name, fname_con)

#%%

save_fig = True
plot_vals = [('idx_raws', 'Rotor-averaged wind speed [m/s]'),
             ('idx_pit', 'Pitch angle [deg]'),
             ('idx_om', 'Rotor speed [rad/s]'),
             ('idx_tbfa', 'Tower-base fore-aft [MNm]'),
             ('idx_tbss', 'Tower-base side-side [MNm]')]


c_orig, c_unc, c_con = '#00008b', '0.7', '#8b0000'
tlim = [100, 700]
lw = 1

plt.rcParams['axes.labelsize'] = 12
plt.rcParams['axes.titlesize'] = 16
plt.rcParams['xtick.labelsize'] = 14
plt.rcParams['ytick.labelsize'] = 14

for iplot, (key, label) in enumerate(plot_vals):
    print(iplot)
    
    tshift = lidar_upwind / wsp  # time-shift to account for shooting upwind
    ishift = int(tshift / 0.01)
    
    # original
    t_orig, data_orig = ds_orig[:, [0, d[key]]].T
    
    # unconstrained
    t_unc = ds_unc[:, 0, 0]
    data_unc = ds_unc[:, d[key], :]
    
    # constrained
    t_con = ds_con[:, 0, 0]
    data_con = ds_con[:, d[key], :]    
    
    fig, ax = plt.subplots(1, 1, num=13, figsize=(7, 3.25), clear=True)
    
    norm = 1e-3 if 'Tower' in label else 1
    
    l1, = ax.plot(t_orig, data_orig*norm, c=c_orig, zorder=10, alpha=0.8, label='Original')
    
    for i in range(6):    
        l2, = ax.plot(t_con + tshift, data_con[:, i]*norm, c=c_con, alpha=0.5, zorder=5, lw=lw)
        l3, = ax.plot(t_unc, data_unc[:, i]*norm, c=c_unc, zorder=0, alpha=0.6, lw=lw)
    
    lcon = Line2D([], [])
    lcon.update_from(l2)
    lcon.set_linewidth(1.5)
    lunc = Line2D([], [])
    lunc.update_from(l3)
    lunc.set_linewidth(1.5)
    
    
    if 'pit' in key:
        ax.legend(handles=[l1, lcon, lunc], labels=['Original', 'Constrained', 'Unconstrained'],
                  fontsize=13)
    # ax.set(xlim=tlim, xlabel='Time [s]', ylabel=label)
    ax.set(xlim=tlim, title=label)
    ax.text(0.99, 0.96, f'({"abcdefghijk"[iplot]})', fontsize=16,
            ha='right', va='top', transform=ax.transAxes)
    fig.tight_layout()
    
    if save_fig:
        fig_dir = 'C:/Users/rink/Documents/publications/2021-08-21_torque2022_pyconturb/paper/figures/overlays/'
        plt.savefig(fig_dir + f'compare_{name}_{wsp}_{key}.png', dpi=150)

