# -*- coding: utf-8 -*-
"""Create turbulence boxes and hawc2 files for each constraint file in a given directory.

Profile functions are default: power profile, iec_sig and kaimal_spectrum. They require:
    - power profile: u_ref, z_ref and alpha
    - iec_sig: turb_class
    - kaimal_spectrum: u_ref
"""
import os
import numpy as np
import pandas as pd
from pyconturb import gen_spat_grid, gen_turb, TimeConstraint
from pyconturb.io import df_to_h2turb
from _pct22utils import fit_power_law, make_turb_htc
from _pct22vals import (coh_dir, con_dir, dtype, kw_dtu10mw, kw_nrel5mw, names, nwid,
                        nht, seed_ext, seed_orig, seeds_con, wsps)


overwrite = False

ds = [kw_dtu10mw, kw_nrel5mw]

for i, (d, name) in enumerate(zip(ds, names)):
    for wsp in wsps:
        print(f'Generating turbulence for {name}, {wsp} m/s...')
        
        fname = name + f'_wsp{wsp}_s{seed_orig}'  # dtu_10mw_wsp11.4_s1001
        
        # constraint file
        con_file = fname + f'_{seed_ext}_ext.con'
        con_path = con_dir + con_file
        con_tc = TimeConstraint(pd.read_csv(con_path, index_col=0))  # load data from csv directly into tc
        con_tc.index = con_tc.index.map(lambda x: float(x) if (x not in 'kxyz') else x)  # index cleaning
        T, nt = con_tc.get_T(), con_tc.get_nt()
        
        # create the spat_df
        y = np.linspace(-d['tb_wid']/2, d['tb_wid']/2, nwid)
        z = np.linspace(-d['tb_ht']/2 + d['z_hub'], d['tb_ht']/2 + d['z_hub'], nht)

        # fit power law
        u_ref, alpha = fit_power_law(con_tc.get_time(), d, lidar_meas=False)
        
        # set turbulence simulation parameters that don't change
        kw = {'T': T, 'nt': nt, 'z_ref': d['z_hub'], 'turb_class': 'A',
              'dtype': dtype, 'u_ref': u_ref, 'alpha': alpha, 'nf_chunk': 150}
        h2turb_dir = d['hawc2_dir'] + 'turb/'
        
        for seed_con in seeds_con:
            print(f'   seed {seed_con}...')
            
            prefix = con_file.replace('.con', f'_cs{seed_con}')
            if os.path.isfile(h2turb_dir + prefix + 'u.bin') and not overwrite:
                print('Turbulence file already exists for', con_file)
                break
        
            # loop through turbulence components
            spat_df = gen_spat_grid([], [])
            turb_df = pd.DataFrame(dtype=dtype)
            for icomp, comp in enumerate('uvw'):
                
                comp_spat_df = gen_spat_grid(y, z, comps=[icomp])
            
                # coherence file name/path
                coh_file = fname + f'{comp}_coh.h5'  # dtu_10mw_wsp11.4_s1001u_coh.h5
                coh_path = coh_dir + coh_file
                
                print('   ', coh_path)
                comp_turb_df = gen_turb(comp_spat_df, seed=seed_con+icomp, coh_file=coh_path,
                                        con_tc=None if icomp else con_tc, verbose=True,
                                        **kw)

                spat_df = pd.concat((spat_df, comp_spat_df), axis=1)
                turb_df = pd.concat((turb_df, comp_turb_df), axis=1)
    
            # save hawc2-style turbulence
            df_to_h2turb(turb_df, spat_df, h2turb_dir, prefix=prefix)
            print(' Turbulence', prefix, 'saved to', h2turb_dir)

            # create the hawc2 file
            htckw = {'wsp': u_ref,
                    'tint': 0.1,  # make nonzero so HAWC2 no freaky freak
                    'prefix': prefix,
                    'nx': con_tc.get_nt(),
                    'ny': nwid,
                    'nz': nht
                    }
            orig_htc = d['hawc2_dir'] + 'htc/' + name + '_master.htc'
            new_htc = d['hawc2_dir'] + 'htc/paper/' + prefix + '.htc'
            make_turb_htc(orig_htc, new_htc, tower_shadow=3, **htckw, **d)
            print(' File', new_htc, 'saved.')
    
