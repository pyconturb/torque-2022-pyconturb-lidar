# -*- coding: utf-8 -*-
"""Zoom into TBFA for DTU 10 MW and NREL 5 MW to compare
"""
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import numpy as np
from _pct22utils import load_hawc2_res
from _pct22vals import (kw_dtu10mw, kw_nrel5mw, lidar_upwind, names, seed_ext,
                        seed_orig, seeds_con)


wsp = 11.4

key = 'idx_tbfa'

ds = [kw_dtu10mw, kw_nrel5mw]

ds_origs, ds_uncs, ds_cons = [], [], []
for i, (d, name) in enumerate(zip(ds, names)):
    # load all needed data
    fname_orig =  f'{name}_wsp{wsp}_s{seed_orig}'
    ds_orig = load_hawc2_res(name, fname_orig)
    ds_unc = np.empty((ds_orig.shape[0], 2, 6))  # nt x 2 x 6
    for iunc, seed_unc in enumerate(range(seed_orig, seed_orig+6)):
        fname_sim =  f'{name}_wsp{wsp}_s{seed_unc}'
        ds_unc[:, :, iunc] = load_hawc2_res(name, fname_sim)[:, [0, d[key]]]
    ds_con = np.empty((ds_orig.shape[0], 2, 6))
    for icon, seed_con in enumerate(seeds_con):
        fname_con =  f'{name}_wsp{wsp}_s{seed_orig}_{seed_ext}_ext_cs{seed_con}'
        ds_con[:, :, icon] = load_hawc2_res(name, fname_con)[:, [0, d[key]]]
    ds_origs.append(ds_orig)
    ds_uncs.append(ds_unc)
    ds_cons.append(ds_con)

del ds_orig, ds_unc, ds_con

#%%

save_fig = False

c_orig, c_unc, c_con = '#00008b', '0.7', '#8b0000'
tlim = [100, 500]
lw = 1
labels = ['DTU 10 MW TbFA', 'NREL 5 MW TbFA']

norm = 1e-3 if 'tbfa' in key else 1

plt.rcParams['axes.labelsize'] = 12
plt.rcParams['axes.titlesize'] = 14

# fig, axs = plt.subplots(2, 1, num=8, clear=True, figsize=(8, 4.5))
fig, axs = plt.subplots(2, 1, num=8, clear=True, figsize=(7, 4.5))

for i, (d, name) in enumerate(zip(ds, names)):
    
    tshift = lidar_upwind / wsp  # time-shift to account for shooting upwind
    ishift = int(tshift / 0.01)
    
    ds_orig = ds_origs[i]
    ds_unc = ds_uncs[i]
    ds_con = ds_cons[i]
    
    ax = axs[i]
    
    # original
    t_orig, data_orig = ds_orig[:, [0, d[key]]].T
    
    # unconstrained
    t_unc = ds_unc[:, 0, 0]
    data_unc = ds_unc[:, 1, :]
    
    # constrained
    t_con = ds_con[:, 0, 0]
    data_con = ds_con[:, 1, :]    
    
    l1, = ax.plot(t_orig, data_orig*norm, c=c_orig, zorder=10, alpha=0.8, label='Original')
    
    for ic in range(6):    
        l2, = ax.plot(t_con + tshift, data_con[:, ic]*norm, c=c_con, alpha=0.5, zorder=5, lw=lw)
        l3, = ax.plot(t_unc, data_unc[:, ic]*norm, c=c_unc, zorder=0, alpha=0.6, lw=lw)
    
    lcon = Line2D([], [])
    lcon.update_from(l2)
    lcon.set_linewidth(1.5)
    lunc = Line2D([], [])
    lunc.update_from(l3)
    lunc.set_linewidth(1.5)
    
    
    if not i:
        # ax.legend(handles=[l1, lcon, lunc], labels=['Original', 'Constrained', 'Unconstrained'],
        #           fontsize='small', bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
        ax.legend(handles=[l1, lcon, lunc], labels=['Original', 'Constrained', 'Unconstrained'],
                  fontsize='medium', bbox_to_anchor=(0., 1.05, 1., .102), loc='lower left',
                  ncol=3, mode="expand", borderaxespad=0)
    # # ax.set(xlim=tlim, xlabel='Time [s]', ylabel=label)
    ax.set(xlim=tlim)
    ax.text(0.99, 0.04, labels[i], fontsize='large',
            ha='right', va='bottom', transform=ax.transAxes)
fig.tight_layout()
    
if save_fig:
    fig_dir = 'C:/Users/rink/Documents/publications/2021-08-21_torque2022_pyconturb/paper/figures/'
    plt.savefig(fig_dir + 'zoom_tbfa.png', dpi=150)

