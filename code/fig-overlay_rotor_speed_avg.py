# -*- coding: utf-8 -*-
"""Pretty visualization of the rotor-average wind speeds with and without constraints.
"""
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import numpy as np
from _pct22utils import load_hawc2_res
from _pct22vals import kw_dtu10mw, kw_nrel5mw, lidar_upwind, seed_ext, seed_orig, seeds_con


name, d = 'dtu_10mw', kw_dtu10mw
# name, d = 'nrel_5mw', kw_nrel5mw
wsp = 11.4
key = 'idx_raws'

tshift = lidar_upwind / wsp  # time-shift to account for shooting upwind
ishift = int(tshift / 0.01)

# original
fname_orig =  f'{name}_wsp{wsp}_s{seed_orig}'
t_orig, data_orig = load_hawc2_res(name, fname_orig)[:, [0, d[key]]].T

# unconstrained
data_unc = np.empty((t_orig.size, 6))
for iunc, seed_unc in enumerate(range(seed_orig, seed_orig+6)):
    fname_sim =  f'{name}_wsp{wsp}_s{seed_unc}'
    t_unc, data_unc[:, iunc] = load_hawc2_res(name, fname_sim)[:, [0, d[key]]].T

# constrained
data_con =  np.empty((t_orig.size, 6))
for icon, seed_con in enumerate(seeds_con):
    fname_con =  f'{name}_wsp{wsp}_s{seed_orig}_{seed_ext}_ext_cs{seed_con}'
    t_con, data_con[:, icon] = load_hawc2_res(name, fname_con)[:, [0, d[key]]].T

#%%

c_orig, c_unc, c_con = '#00008b', '0.7', '#8b0000'
tlim = [100, 700]
lw = 1
save_fig = False

fig, ax = plt.subplots(1, 1, num=13, figsize=(7, 3), clear=True)

l1, = ax.plot(t_orig, data_orig, c=c_orig, zorder=10, alpha=0.8, label='Original')

for i in range(6):    
    l2, = ax.plot(t_con + tshift, data_con[:, i], c=c_con, alpha=0.5, zorder=5, lw=lw)
    l3, = ax.plot(t_unc, data_unc[:, i], c=c_unc, zorder=0, alpha=0.6, lw=lw)

lcon = Line2D([], [])
lcon.update_from(l2)
lcon.set_linewidth(1.5)
lunc = Line2D([], [])
lunc.update_from(l3)
lunc.set_linewidth(1.5)


ax.legend(handles=[l1, lcon, lunc], labels=['Original', 'Constrained', 'Unconstrained'],
          fontsize='small')
ax.set(xlim=tlim, xlabel='Time [s]', ylabel='Rotor-avg. wind speed [m/s]',
       )
fig.tight_layout()

if save_fig:
    fig_dir = 'C:/Users/rink/Documents/publications/2021-08-21_torque2022_pyconturb/paper/figures/'
    plt.savefig(fig_dir + 'compare_rotor_avg_wsp.png', dpi=150)
    print(f'Figure saved to {fig_dir}.')

