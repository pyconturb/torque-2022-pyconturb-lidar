# -*- coding: utf-8 -*-
"""Process the MAE and EDEL values for all simulations
"""
import numpy as np
import pandas as pd
from wetb.fatigue_tools.fatigue import eq_load
from wetb.hawc2.Hawc2io import ReadHawc2
from _pct22utils import load_hawc2_res
from _pct22vals import (lidar_upwind, kw_dtu10mw, kw_nrel5mw, names, nbeams, 
                        seed_ext, seed_orig, seeds_con, wsps)


ds = [kw_dtu10mw, kw_nrel5mw]

chans = [('idc_freewind1', 1), ('idc_freewind2', 1), ('idc_freewind3', 1), ('idc_freewind4', 1),
         ('idx_raws', 1), ('idx_om', 1), ('idx_pit', 1),
         ('idx_tbfa', 4), ('idx_tbss', 4), ('idx_tttl', 4), ('idx_ttrl', 4),
         ('idx_flp1', 10), ('idx_edg1', 10), ('idx_flp2', 10), ('idx_edg2', 10),
         ('idx_flp3', 10), ('idx_edg3', 10)]
neq = 600
nsims_per_type = 6  # no. of simulations per unconstrained/constrained

# initialize the dataset
stats_df = pd.DataFrame(columns=['name', 'wsp', 'type', 'channel',
                                 'mean', 'std', 'mae', 'del'])

# loop over turbines
irow = 0
for i, (d, name) in enumerate(zip(ds, names)):
    
    # get the freewind index
    idcs_freewind = [d['idx_wsp1'] + i*3 for i in range(nbeams)]  # horizontal wind speeds
    for ibeam in range(1, nbeams + 1):
        d[f'idc_freewind{ibeam}'] = idcs_freewind[ibeam - 1]  # free wind index
    
    # loop over wind speeds
    for j, wsp in enumerate(wsps):
        print(f'Processing statistics for {name}, {wsp} m/s...')
        
        tshift = lidar_upwind / wsp  # time-shift to account for shooting upwind
        ishift = int(tshift / 0.01)

        # load the data from the original simulation
        fname_orig =  f'{name}_wsp{wsp}_s{seed_orig}'
        data_orig = load_hawc2_res(name, fname_orig)
        t_orig = data_orig[:, 0]
        
        
        # loop over the channels to process
        for key, m in chans:
            print(f'  ...key {key}...')
        
            # isolate the original channel from its dataset
            x_orig = data_orig[:, d[key]]
            
            # calculate the DEL, abs_avg, and stddev of the original channel
            del_orig = eq_load(x_orig, m=m, neq=neq)[0][0]
            std_orig = np.std(x_orig)
            
            # add row for original channel
            stats_df.loc[irow, :] = [name, wsp, 'orig', key,
                                     x_orig.mean(), x_orig.std(), 0, del_orig]
            irow += 1
            
            # loop over the unconstrained and constrained seeds
            for k in range(nsims_per_type*2):
                
                if k // nsims_per_type:  # unconstrained
                    sim_type = 'unc'
                    seed_unc = seed_orig + k + 1 - nsims_per_type
                    fname_sim =  f'{name}_wsp{wsp}_s{seed_unc}'
                    
                else:  # constrained
                    sim_type = 'con'
                    seed_con = seeds_con[k]
                    fname_sim =  f'{name}_wsp{wsp}_s{seed_orig}_{seed_ext}_ext_cs{seed_con}'
                
                # load the data
                data_sim = load_hawc2_res(name, fname_sim)
                
                # isolate the time and data channels
                t_sim = data_sim[:, 0]
                x_sim = data_sim[:, d[key]]
                
                # values to return
                del_sim = eq_load(x_sim, m=m, neq=neq)[0][0]
                mae = np.mean(np.abs(x_orig[ishift:] - x_sim[:-ishift]))
                
                # make row and add to dataset
                stats_df.loc[irow, :] = [name, wsp, sim_type, key,
                                         x_sim.mean(), x_sim.std(), mae, del_sim]
                irow += 1

stats_df.to_csv('torque2022_stats.csv')
