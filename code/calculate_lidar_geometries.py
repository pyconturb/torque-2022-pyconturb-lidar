# -*- coding: utf-8 -*-
"""Determine where the lidar is located for HAWC2 input file
"""
import matplotlib.pyplot as plt
import numpy as np
from _pct22vals import kw_dtu10mw, kw_nrel5mw, nbeams, half_cone, nwid, nht

# d_rotor = 2*61.5
# z_hub = 90

rayleigh_length = 6.5  # rayleigh length of the beam [m]
half_width = 6  # half-width of the integration value
no_int_pts = 100 # number of integration points
focus_length = 100  # focus length measured along the beam [m]

kws = [kw_dtu10mw, kw_nrel5mw]
names = ['DTU 10 MW', 'NREL 5 MW']

plt.style.use('paper.mplstyle')

# initialize figure
fig, axs = plt.subplots(1, 2, num=1, clear=True, figsize=(8, 4))

for it, (d, name) in enumerate(zip(kws, names)):
    print(f'\nPARAMETERS FOR {name}\n')

    # rotor center location
    x_rc = 0
    y_rc = -d['shaft_length'] * np.cos(d['tilt'])
    z_rc = -d['z_hub']
    
    print(f'\nRotor location: {x_rc:.3f} {y_rc:.3f} {z_rc:.3f}')

    # lidar displacement from rotor center (collocated)
    dx_rc = 0  # distance from rotor center in global x coordinates [m]
    dy_rc = 0 # distance in global y (pos. downwind) [m]
    dz_rc = 0  # assumed height above the rotor center (pos.downwards) [m]

    # lidar location
    x_ld = x_rc + dx_rc
    y_ld = y_rc + dy_rc
    z_ld = z_rc + dz_rc
    
    print(f'Lidar location: {x_ld:.3f} {y_ld:.3f} {z_ld:.3f}\n')

    for i in range(nbeams):
        ang = 180/nbeams + 360/nbeams*i
        print(f'   aero nacelle_lidar {dx_rc:.4f} {dy_rc:.4f} {dz_rc:.4f}' + 
              f' {half_cone*180/np.pi:.0f} {ang:.0f} {focus_length:.1f}' +
              f' {rayleigh_length:.1f} {half_width:.1f} {no_int_pts:.0f}' +
              f' {i+1:.0f} # beam_{i+1} ;')

    # locations of beam (without tower deflection)
    beam_locs = np.empty((3, nbeams))
    Rz = focus_length * np.sin(half_cone)
    for i in range(nbeams):
        ang = 180/nbeams + 360/nbeams*i  # counter-clockwise looking downwind, 0 at top
        x = x_ld + Rz * np.sin(ang*np.pi/180)
        y = y_ld - focus_length * np.cos(half_cone)
        z = z_ld - Rz * np.cos(ang*np.pi/180)
        print(f'   wind free_wind 1 {x:.4f} {y:.4f} {z:.4f} # wsp_beam{i+1} ;')
        beam_locs[:, i] = [x, y, z]

    # get grid locations
    y = np.linspace(-d['tb_wid']/2, d['tb_ht']/2, nwid)
    z = np.linspace(-d['tb_ht']/2 + d['z_hub'], d['tb_ht']/2 + d['z_hub'], nht)
    Y, Z = np.meshgrid(y, z)

    # plot the rotor, the beam locations and the grid locations
    ax = axs[it]
    azim = np.linspace(0, 2*np.pi, 401)
    ax.plot([-3, 3, 1.5, -1.5, -3], [0, 0, d['z_hub'], d['z_hub'], 0], c='0.3', zorder=-3)  # tower
    ax.plot(d['d_rotor']/2*np.cos(azim), d['d_rotor']/2*np.sin(azim) + d['z_hub'], c='0.6')  # rotor
    circle1 = plt.Circle((0, d['z_hub']), 2*2, facecolor='w', edgecolor='0.3', lw=1.5)
    ax.add_patch(circle1)  # hub
    ax.plot(Y.flatten(), Z.flatten(), 'o', ms=1, alpha=0.5)  # tbox
    
    for i in range(nbeams):
        ax.plot(-beam_locs[0, :], -beam_locs[2, :], 'o', mfc='0.9', mec='#990F02', ms=3)  # -x, -z
        ax.annotate(f'{i+1}', (-beam_locs[0, i], -beam_locs[2, i]), 
                      (-beam_locs[0, i] - 13*np.sin(np.pi*i/2 + np.pi/4),
                      -beam_locs[2, i] + 6*np.cos(np.pi*i/2 + np.pi/4)),
                      xycoords='data',
                      ha='center', va='center', fontsize='medium')
    ax.axis('equal')
    ax.set_ylim([0, 220])
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    if it > 0:
        ax.spines['left'].set_visible(False)
        ax.set_yticks([])
    ax.set_title(name)
# fig.suptitle('Looking downwind')
fig.tight_layout()

save_fig = 0
if save_fig:
    fig_dir = 'C:/Users/rink/Documents/publications/2021-08-21_torque_pyconturb/abstract/figures/'
    plt.savefig(fig_dir + 'lidar_geometries.png', dpi=150)