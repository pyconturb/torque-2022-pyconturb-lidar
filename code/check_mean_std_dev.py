# -*- coding: utf-8 -*-
"""
Check whether we recover the correct mean and standard deviation of signals
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from _pct22vals import wsps, names

stats_df = pd.read_csv('torque2022_stats.csv', index_col=0)


chans = ['idc_freewind1', 'idc_freewind2', 'idc_freewind3', 'idc_freewind4',
         'idx_rawp', 'idx_om', 'idx_pit', 'idx_tbfa', 'idx_tbss', 'idx_tttl', 'idx_ttrl']


kw = {'mfc': 'w', 'alpha': 0.9}
markers = 'oxs'


fig_m, axs_m = plt.subplots(4, 3, num=15, figsize=(12, 8), clear=True)
fig_s, axs_s = plt.subplots(4, 3, num=16, figsize=(12, 8), clear=True)

nsims_per_type = 6
rated_offset = 12
con_offset = 4


for iax, chan in enumerate(chans):  # channels
    
    ax1m = axs_m[iax // 3, iax % 3]
    # ax2m = ax1m.twinx()
    ax2m = ax1m
    ax1s = axs_s[iax // 3, iax % 3]
    ax2s = ax1s.twinx()
    
    for j, wsp in enumerate(wsps):  # wind speeds
        axm = [ax1m, ax2m][j]
        axs = [ax1s, ax2s][j]
        
        for k, name in enumerate(names):  # wind turbines
            c = 'rg'[k]
            
            orig_df = stats_df[(stats_df.channel == chan) &
                              (stats_df.wsp == wsp) &
                              (stats_df.name == name) &
                              (stats_df.type == 'orig')]
            unc_df = stats_df[(stats_df.channel == chan) &
                                  (stats_df.wsp == wsp) &
                                  (stats_df.name == name) &
                                  (stats_df.type == 'unc')]
            con_df = stats_df[(stats_df.channel == chan) &
                                  (stats_df.wsp == wsp) &
                                  (stats_df.name == name) &
                                  (stats_df.type == 'con')]
            
            mean_orig = orig_df['mean'].values
            mean_unc = np.mean(unc_df['mean'].values)
            mean_con = np.mean(con_df['mean'].values)
            std_orig = orig_df['std'].values
            std_unc = np.sqrt(np.mean(unc_df['std'].values**2))
            std_con = np.sqrt(np.mean(con_df['std'].values**2))
            
            orig_x = k * con_offset + j * rated_offset
            unc_x = orig_x + 1
            con_x = unc_x + 1
            
            normm = 1 / mean_orig
            offsetm = -1
            
            axm.plot(orig_x, mean_orig * normm + offsetm, c=c, marker='o', **kw)
            axm.plot(unc_x, mean_unc * normm + offsetm, c=c, marker='x', **kw)
            axm.plot(con_x, mean_con * normm + offsetm, c=c, marker='s', **kw)
            
            axs.plot(orig_x, std_orig, c=c, marker='o', **kw)
            axs.plot(unc_x, std_unc, c=c, marker='x', **kw)
            axs.plot(con_x, std_con, c=c, marker='s', **kw)
            
        axm.set(title=chan, xticks=[])
        axs.set(title=chan, xticks=[])

fig_m.suptitle('Means')
fig_s.suptitle('Standard deviations')
fig_m.tight_layout()
fig_s.tight_layout()
 