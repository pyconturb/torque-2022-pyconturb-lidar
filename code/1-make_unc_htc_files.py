# -*- coding: utf-8 -*-
"""make htc files to create unconstrained turbulence
"""
from _pct22utils import make_unc_turb
from _pct22vals import kw_dtu10mw, kw_nrel5mw, wsps, iref


if __name__ == '__main__':
    for wsp in wsps:
        tint = 0.16 * (0.75 * wsp + 5.6) / wsp  # assuming turb class A [KEEP NTM PCT CAN'T DO ETM]
        kw = {'wsp': wsp,
              'tint': tint}
        for i in range(2):
            if i == 0:  # dtu 10 mw
                d = kw_dtu10mw
                kw['tb_wid'] = d['tb_wid']
                kw['tb_ht'] = d['tb_ht']
                orig_htc = '../dtu_10mw/htc/dtu_10mw_master.htc'
                save_dir = '../dtu_10mw/htc/paper/'
            else:  # nrel 5 mw
                d = kw_nrel5mw
                kw['tb_wid'] = d['tb_wid']
                kw['tb_ht'] = d['tb_ht']
                orig_htc = '../nrel_5mw/htc/nrel_5mw_master.htc'
                save_dir = '../nrel_5mw/htc/paper/'
            make_unc_turb(orig_htc, save_dir, verbose=True, **kw)
