# -*- coding: utf-8 -*-
"""Compare the different wind signals.
"""
import matplotlib.pyplot as plt
import numpy as np
from wetb.hawc2.Hawc2io import ReadHawc2
from _pct22utils import (wrap_time, adjust_hf, fit_power_law, kaimal_psd, extend_spectrum, 
                         calculate_psd, load_lidar_meas, get_beam, shift_time)
from _pct22vals import half_cone, nbeams, fc, kw_dtu10mw, seed_ext


# use dtu 10 mw values @ 11.4 m/s
d, wsp = kw_dtu10mw, 11.4
lidar_path = f'../lidar/dtu_10mw_wsp{wsp}_s1001.npy'  # lidar file
hawc2_path = (d['hawc2_dir'] + f'res/dtu_10mw_wsp{wsp}_s1001.hdf5')  # for free wind comparison

# beam to focus on
ibeam = 4

# load the HAWC2 file
read_h2 = ReadHawc2(hawc2_path)
data = read_h2.ReadGtsdf()

# wrap data, get free wind
idcs_freewind = [d['idx_wsp1'] + i*3 for i in range(nbeams)]  # horizontal wind speeds
data = wrap_time(data, 0)
t_free, u_free = data[:, 0], data[:, idcs_freewind][:, ibeam-1]

# load the lidar file (both w/wrapping and one w/half-cone scaling)
lidar_meas_raw = load_lidar_meas(lidar_path, hc_scale=False)
lidar_meas = load_lidar_meas(lidar_path, hc_scale=True)


# fit mean profile (need u_ref for Kaimal fit)
u_ref, alpha = fit_power_law(lidar_meas, d)
print(f'\nu_ref: {u_ref:.3f}, alpha: {alpha:.3f}/n')

# desired time vector
tsamp = lidar_meas[:, 0]  # sampling times
tscan = tsamp[::nbeams]  # full-scan times

# get time and u_beam (filtering nans)
t_beam_raw, u_beam_raw = get_beam(lidar_meas_raw, ibeam)
t_beam, u_beam = get_beam(lidar_meas, ibeam)

# get the PSD of the mean wind and raw lidar
f_free, S_free = calculate_psd(t_free, u_free)
f_lidar, S_lidar = calculate_psd(t_beam, u_beam)

# adjust the start time of each beam using a time shift
t_shift, u_shift = shift_time(t_beam, u_beam)

# perform high-frequency adjustment
ulidar_hf, flidar, _, Slidar_hf = adjust_hf(t_shift, u_shift, fc, return_S=True)

# extend the spectrum
u_ref, alpha = fit_power_law(lidar_meas, d)
ulidar_ext, fext, Sext, Xext, sig = extend_spectrum(t_shift, ulidar_hf, flidar,
                                                    Slidar_hf, u_ref, tsamp,
                                                    return_S=True, scale_var=True,
                                                    seed=seed_ext)

# compare psd values
L, df = 340.2, 1/600
Stheo = kaimal_psd(f_free, sig, L, u_ref, df)

#%%

save_fig = False

t_zoom = [100, 120]
ylim = [8, 14]

plt.style.use('paper.mplstyle')

colors = [_['color'] for _ in list(plt.rcParams['axes.prop_cycle'])]

# set up the figure
fig = plt.figure(num=5, clear=True, figsize=(11, 4.))

ax = plt.subplot(221)
l1, = ax.plot(t_free, u_free, label='Free wind (u)', zorder=5)
l2, = ax.plot(t_beam_raw, u_beam_raw, 'o-', mfc='w', alpha=0.8, label='Lidar LOS')
ax.plot(t_shift, u_shift, 'o-', mfc='w', alpha=0.8, label='Scaled, time-shifted')
ax.set_xlim(t_zoom)
ax.set_ylim(ylim)
ax.legend(fontsize='small')
ax.set_ylabel('Velocity [m/s]')
ax.set_xticks([100, 105, 110, 115, 120])
ax.set_xticklabels([])

ax = plt.subplot(223)
ax.plot(t_free, u_free, label='Longitudinal turbulence', zorder=5)
l3, = ax.plot(tsamp, ulidar_ext, 'o-', mfc='w', c=colors[3], alpha=0.8, label='Extended spectrum')
ax.set_xlim(t_zoom)
ax.set_ylim(ylim)
ax.set_xlabel('Time [s]')
ax.set_ylabel('Velocity [m/s]')
ax.legend(fontsize='small')
ax.set_xticks([100, 105, 110, 115, 120])

ax = plt.subplot(122)
ax.loglog(f_free[1:], S_free[1:], zorder=1, alpha=0.7)
ax.loglog(f_lidar[1:], S_lidar[1:], '-o', alpha=0.7, lw=0.7, mfc='w', ms=2, zorder=4)
ax.loglog(fext[1:], Sext[1:], '-o', alpha=0.7, mfc='w', lw=0.7, ms=2, zorder=2,  c=colors[3])
l4, = ax.loglog(f_free[1:], Stheo[1:], '--', c='0.6', zorder=0)
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('PSD [(m/s)$^2$/Hz]')
ax.set_xlim([1e-3, 1e1])
ax.set_ylim([1e-4, ax.get_ylim()[1]])
ax.legend(handles=[l1, l2, l3, l4], labels=['Free wind (u)', 'Lidar LOS', 'Extended spectrum', 'Kaimal'])

fig.tight_layout()
plt.show()

if save_fig:
    fig_dir = 'C:/Users/rink/Documents/publications/2021-08-21_torque2022_pyconturb/paper/figures/'
    plt.savefig(fig_dir + 'compare_lidar_con.png', dpi=150)
    print(f'Fig saved to {fig_dir}')