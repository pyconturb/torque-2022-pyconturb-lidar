# -*- coding: utf-8 -*-
"""Values for Torque 2022 PyConTurb paper
"""
import numpy as np


# kw = {'idx_shft': 1,  # index of shaft azim in deg
#       'r_lidar': 4,  # radial distance rotor center to lidar center [m]
#       'c_rlidar': 5.38,  # chord at r_lidar [m]
#       'idx_lidar1': 108,  # index of first lidar line of sight
#       'idx_wsp1': 129,  # index of first free wsp at beam locations
#       }

wsps = [11.4, 24]
iref = 0.16  # turbulence class A
con_dir = '../constraints/'
coh_dir = 'D:/torque2022_coherence_files/'
lidar_dir = '../lidar/'
seed_ext = 7971  # for extending
seed_orig = 1001  # original unconstrained seed
dtype = np.float32
names = ['dtu_10mw', 'nrel_5mw']
seeds_con = [586, 1337, 3522, 4756, 4967, 9891]  # next 5

# lidar (processing) parameters
nbeams = 4  # number of lidar beams
dt_scan = 1  # sample time for finishing entire scan
half_cone = 15 * np.pi/180  # half-cone [rad]
focus_length = 100  # focus length measured along the beam [m]
fc = 0.1  # edge frequency above which we apply compensation [Hz]
lidar_upwind = focus_length * np.cos(half_cone)

# turbulence box parameters
nwid, nht = 32, 32

# dtu 10 mw parameters
kw_dtu10mw = dict(tilt = 5*np.pi/180,  # tilt [rad]
                  shaft_length = 7.1,  # shaft length [m]
                  z_hub = 119,  # hub height [m]
                  d_rotor = 178,  # rotor diameter [m]
                  tb_wid = 180,  # width of turbulence box [m]
                  tb_ht = 180,  # height of turbulence box [m]
                  idx_shft = 1,  # index of shaft azimuth in deg
                  idx_lidar1 = 109,  # index of first lidar LOS
                  idx_wsp1 = 130,  # index of first free wsp at beam locs
                  idx_raws = 16,  # rotor-averaged wind speed
                  idx_om = 9,  # rotor speed
                  idx_pit = 3,  # pitch angle
                  idx_tbfa = 17,  # tower-base fore-aft
                  idx_tbss = 18,  # tower-base side-side
                  idx_tttl = 20,  # tower-top tilt
                  idx_ttrl = 21,  # tower-top roll
                  idx_flp1 = 26,  # flap 1
                  idx_edg1 = 27,  # edge 1
                  idx_flp2 = 29,  # flap 2
                  idx_edg2 = 30,  # edge 2
                  idx_flp3 = 32,  # flap 3
                  idx_edg3 = 33,  # edge 3
                  beam_locs = np.array([[18.30127019, 18.30127019, -18.30127019, -18.30127019],
                                       [-103.66556499, -103.66556499, -103.66556499, -103.66556499],
                                       [-137.30127019, -100.69872981, -100.69872981, -137.30127019]]),
                  hawc2_dir = 'C:/Users/rink/git/G-pyconturb/torque-2022-pyconturb-lidar/dtu_10mw/',
                                      )

# nrel 5 mw parameters
kw_nrel5mw = dict(tilt = 5*np.pi/180,  # tilt [rad]
                  shaft_length = 5.0191,  # shaft length [m]
                  z_hub = 90,  # hub height [m]
                  d_rotor = 126,  # rotor diameter [m]
                  tb_wid = 130,  # width of turbulence box [m]
                  tb_ht = 130,  # height of turbulence box [m]
                  idx_shft = 1,  # index of shaft azimuth in deg
                  idx_lidar1 = 72,  # index of first lidar LOS
                  idx_wsp1 = 93,  # index of first free wsp at beam locs
                  idx_raws = 16,  # rotor-averaged wind speed
                  idx_om = 9,  # rotor speed
                  idx_pit = 3,  # pitch angle
                  idx_tbfa = 17,  # tower-base fore-aft
                  idx_tbss = 18,  # tower-base side-side
                  idx_tttl = 20,  # tower-top tilt
                  idx_ttrl = 21,  # tower-top roll
                  idx_flp1 = 26,  # flap 1
                  idx_edg1 = 27,  # edge 1
                  idx_flp2 = 29,  # flap 2
                  idx_edg2 = 30,  # edge 2
                  idx_flp3 = 32,  # flap 3
                  idx_edg3 = 33,  # edge 3
                  beam_locs = np.array([[  18.30127019,   18.30127019,  -18.30127019,  -18.30127019],
                                       [-101.59258344, -101.59258344, -101.59258344, -101.59258344],
                                       [-108.30127019,  -71.69872981,  -71.69872981, -108.30127019]]),
                  hawc2_dir = 'C:/Users/rink/git/G-pyconturb/torque-2022-pyconturb-lidar/nrel_5mw/'
                                      )

# indices for line-of-sight and wind speeds
# idcs_los = [kw['idx_lidar1'] + i*5 for i in range(nbeams)]  # lines of sight
# idcs_freewind = [kw['idx_wsp1'] + i*3 for i in range(nbeams)]  # horizontal wind speeds

# beam locations in global hawc2 coordinates
# beam_locs = np.array([[  18.30127019, -100.66556499, -141.30007596],
#                       [  18.30127019, -100.66556499, -104.69753558],
#                       [ -18.30127019, -100.66556499, -104.69753558],
#                       [ -18.30127019, -100.66556499, -141.30007596]])

