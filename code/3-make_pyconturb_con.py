# -*- coding: utf-8 -*-
"""Load lidar file and save two sets of constraints:
    1. Raw, with even time step - 1 Hz (RE)
    2. High-frequency correction - 1 Hz (HF)
"""
import os
import numpy as np
from _pct22utils import (load_lidar_meas, adjust_hf, get_beam, is_even, shift_time,
                         save_timecon, fit_power_law, extend_spectrum)
from _pct22vals import nbeams, fc, kw_dtu10mw, kw_nrel5mw, names, wsps, con_dir, seed_ext



ds = [kw_dtu10mw, kw_nrel5mw]
seed_unc = 1001  # original

for i, (d, name) in enumerate(zip(ds, names)):
    for wsp in wsps:
        fname = name + f'_wsp{wsp}_s{seed_unc}'
        
        lidar_path = '../lidar/' + fname + '.npy'
    
        # ---------- pre-processing ----------
        
        lidar_meas = load_lidar_meas(lidar_path)
    
        # ---------- resample each beam to the same scanning time series ----------
        
        tsamp = lidar_meas[:, 0]  # sampling times
        tscan = tsamp[::nbeams]  # full-scan times
        ntscan = tscan.shape[0]  # number of scan points
    
        u_re = np.empty((ntscan, nbeams + 1))  # raw, resampled to even time steps
        u_hf = np.empty((ntscan, nbeams + 1))  # with high-frequency adjustment
        u_ext = np.empty((tsamp.size, nbeams + 1))  # single extension
        u_re[:, 0] = tscan
        u_hf[:, 0] = tscan
        u_ext[:, 0] = tsamp
    
        # process each beam
        for ibeam in range(1, nbeams+1):
        
            # get time and u_beam (filtering nans)
            t_beam, u_beam = get_beam(lidar_meas, ibeam)
        
            # stop code if time step is not even
            if not is_even(t_beam):
                raise ValueError('Time step is not even!')
        
            # adjust the start time of each beam using a time shift
            t_shift, u_shift = shift_time(t_beam, u_beam)
            np.testing.assert_allclose(tscan, t_shift)
            u_re[:, ibeam] = u_shift
        
            # perform high-frequency adjustment
            ulidar_hf, flidar, _, Slidar_hf = adjust_hf(t_shift, u_shift, fc, return_S=True)
            u_hf[:, ibeam] = ulidar_hf
        
            # extend the spectrum
            u_ref, alpha = fit_power_law(lidar_meas, d)
            ulidar_ext, fext, Sext, Xext, sig = extend_spectrum(t_shift, ulidar_hf, flidar,
                                                                Slidar_hf, u_ref, tsamp,
                                                                return_S=True, scale_var=True,
                                                                seed=seed_ext)
            u_ext[:, ibeam] = ulidar_ext
    
    
        # make constraint file for u_re
        re_con_path = con_dir + os.path.basename(lidar_path).replace('.npy', '_re.con')
        timecon_re = save_timecon(re_con_path, u_re, d['beam_locs'])
        print('Con file', re_con_path, 'saved.')
        
        # make constraint file for u_hf
        hf_con_path = con_dir + os.path.basename(lidar_path).replace('.npy', '_hf.con')
        timecon_hf = save_timecon(hf_con_path, u_hf, d['beam_locs'])
        print('Con file', hf_con_path, 'saved.')
    
        # make constraint file for u_ext
        ext_con_path = con_dir + os.path.basename(lidar_path).replace('.npy', f'_{seed_ext}_ext.con')
        timecon_ext = save_timecon(ext_con_path, u_ext, d['beam_locs'])
        print('Con file', ext_con_path, 'saved.')
