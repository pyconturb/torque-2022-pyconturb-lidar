# -*- coding: utf-8 -*-
"""Get lidar signal from HAWC2 results
"""
import os
import numpy as np
from wetb.hawc2.Hawc2io import ReadHawc2
from _pct22utils import sample_cw_lidar
from _pct22vals import kw_dtu10mw, kw_nrel5mw, nbeams, dt_scan, wsps, lidar_dir


if __name__ == '__main__':
    
    save_dir = lidar_dir
    ds = [kw_dtu10mw, kw_nrel5mw]
    names = ['dtu_10mw', 'nrel_5mw']
    seed = 1001  # only make lidar "data" for one seed
    for i, (d, name) in enumerate(zip(ds, names)):
        for wsp in wsps:
            fname = name + f'_wsp{wsp}_s{seed}'
            hawc2_path = d['hawc2_dir'] + 'res/' + fname + '.hdf5'
    
            # get the indices we need
            idcs_los = [d['idx_lidar1'] + i*5 for i in range(nbeams)]  # lines of sight
            idcs_freewind = [d['idx_wsp1'] + i*3 for i in range(nbeams)]  # horizontal wind speeds
    
            # load the HAWC2 file
            read_h2 = ReadHawc2(hawc2_path)
            data = read_h2.ReadGtsdf()
        
            # sample and save unblocked
            lidar_noblock = sample_cw_lidar(data, idcs_los, dt_scan)
            np.save(save_dir + fname + '.npy', lidar_noblock)
            
            # # sample and save blocked [nope, not for thise paper]
            # lidar_block = sample_cw_lidar(data, idcs_los, t_scan, blockage=True, **kw)
            # np.save(save_dir + fname + '_blocked.npy', lidar_block)
