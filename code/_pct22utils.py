# -*- coding: utf-8 -*-
"""Reused functions for Torque 2022 PyConTurb paper
"""
import os
import numpy as np
import pandas as pd
from scipy.optimize import minimize
from wetb.hawc2 import HTCFile
from wetb.hawc2.Hawc2io import ReadHawc2
from _pct22vals import (half_cone, kw_dtu10mw, kw_nrel5mw)


def adjust_hf(t_lidar, u_lidar, fc, scale_var=True, alpha_des=-5/3, return_S=False):
    """Scale to correct roll-off above given cut-off frequency.
    Optional to correct the variance of the output signal."""
    # get psd
    f, S, X = calculate_psd(t_lidar, u_lidar, return_X=True)
    # make a mask for high frequency and non-zero PSD
    mask = (f > fc) & (S > 0)
    # take log
    logf = np.log10(f[mask])
    logS = np.log10(S[mask])
    # calculate slope correction
    x = fit_straight_psd(f[mask], S[mask])
    slope_correction = alpha_des - x[0]
    # scale the high frequency part of the spectrum
    logS_sc = logS + (logf - np.log10(fc)) * slope_correction
    # get spectrum and fourier transform
    S_hf = S.copy()
    S_hf[mask] = 10**logS_sc
    X_hf = X.copy()
    X_hf[mask] *= np.sqrt(S_hf[mask] / S[mask])
    # rescale if normalize variance
    if scale_var:
        psd_scale = sum(S[1:]) / sum(S_hf[1:])
        S_hf[1:] *= psd_scale
        X_hf[1:] *= np.sqrt(psd_scale)
    # transform back to time domain
    u_hf = np.fft.irfft(X_hf) * t_lidar.size
    if return_S:
        return u_hf, f, S, S_hf
    else:
        return u_hf


def make_unc_turb(orig_htc, save_dir, tower_shadow=3, verbose=False, **kw):
    """Make unconstrained turbulence files. Mann turbulence, power law. 8192 x 32 x 32.

    Required keyword arguments: wsp, tint, tb_wid, tb_ht
    """
    nx, ny, nz = 8192, 32, 32
    tstart, tstop = 100 - 0.01, 700 - 0.01
    # get original filename
    orig_fname = os.path.basename(orig_htc).replace('.htc', '')  # dtu_10mw_master
    # loop over seeds
    for seed in np.arange(1001, 1008):
        htc = HTCFile(orig_htc)
        U, tint = kw['wsp'], kw['tint']
        wid, ht = kw['tb_wid'], kw['tb_ht']
        fname = orig_fname.replace('_master', '')  # dtu_10mw_wsp11.4_s1001
        fname += f'_wsp{U}_s{seed}'
        # set simulation time and file names
        htc.simulation.time_stop = 700
        htc.simulation.logfile = f'./log/{fname}.log'
        htc.output.filename = f'./res/{fname}'
        # add the wind
        htc.wind.wsp = U  # mean wind speed
        htc.wind.tint = tint  # turbulence intensity
        htc.wind.shear_format = [3, 0.2]  # power-law shear
        htc.wind.turb_format = 1  # mann
        htc.wind.scale_time_start = 100
        htc.wind.tower_shadow_method = tower_shadow
        if 'mann' in htc.wind:
            del htc.wind.mann
        mann = htc.wind.add_section('mann')
        mann.add_line('create_turb_parameters', [33.6, 1.0, 3.9, seed, 1])
        mann.add_line('filename_u', [f'./turb/{fname}_turb_u.bin'])
        mann.add_line('filename_v', [f'./turb/{fname}_turb_v.bin'])
        mann.add_line('filename_w', [f'./turb/{fname}_turb_w.bin'])
        mann.add_line('box_dim_u', [nx, U * 600 / nx])  # repeat every 600 seconds
        mann.add_line('box_dim_v', [ny, wid / (ny - 1)])
        mann.add_line('box_dim_w', [nz, ht / (nz - 1)])
        # update the simulation time
        htc.output.time = [tstart, tstop]
        # save the new file
        save_path = save_dir + fname + '.htc'
        htc.save(save_path)
        if verbose:
            print('Saved file', save_path)


def mask_data(t, x, tlim):
    mask = (t >= tlim[0]) & (t <= tlim[1])
    return t[mask], x[mask]


def calculate_psd(t, x, return_X=False):
    """Get the one-sided PSD of signal"""
    dt = (t[-1] - t[0]) / (t.size - 1)
    T = t[-1] - t[0] + dt
    df = 1 / T
    f = np.fft.rfftfreq(x.size, d=dt)
    X = np.fft.rfft(x) / x.size
    Sx = 2 * np.abs(X)**2 / df
    if return_X:
        return f, Sx, X
    else:
        return f, Sx


def extend_spectrum(tlidar, ulidar, flidar, Slidar, u_ref, t_ext, return_S=False,
                    scale_var=True, seed=None):
    """Extend the power spectrum to a new time vector"""
    L = 340.2  # kaimal length scale
    nf_lidar = flidar.size

    # calculate the new frequency vector
    dt = (t_ext[-1] - t_ext[0]) / (t_ext.size - 1)
    T = t_ext[-1] - t_ext[0] + dt
    fext = np.fft.rfftfreq(t_ext.size, dt)
    df = (fext[-1] - fext[0])/(fext.size - 1)

    # fit kaimal spectrum
    sig = fit_kaimal(flidar[1:], Slidar[1:], u_ref, df)

    # extend fft using kaimal spectrum
    Sext, Xext = np.empty(fext.size), np.empty(fext.size, dtype=complex)
    np.random.seed(seed)
    # get spectrum and fourier vector in extended region
    Shf = kaimal_psd(fext[nf_lidar:], sig, L, u_ref, df)
    Xhf = np.sqrt(Shf * df / 2) * np.exp(1j * 2*np.pi * np.random.rand(Shf.size))
    # lower frequencies -- same as lidar
    Sext[:nf_lidar] = Slidar
    Xlidar = np.fft.rfft(ulidar) / ulidar.size
    Xext[:nf_lidar] = Xlidar
    # higher frequencies -- extended
    Sext[nf_lidar:] = Shf
    Xext[nf_lidar:] = Xhf

    # rescale if normalize variance
    if scale_var:
        S_scale = sum(Slidar[1:]) / sum(Sext[1:])
        Sext[1:] *= S_scale
        Xext[1:] *= np.sqrt(S_scale)

    # bring back to time domain
    ulidar_ext = np.fft.irfft(Xext) * t_ext.size

    # adjust for any time offset in tlidar
    t_ext = (t_ext + tlidar[0]) % T
    idx = np.argsort(t_ext)
    ulidar_ext = ulidar_ext[idx]

    if return_S:
        return ulidar_ext, fext, Sext, Xext, sig
    else:
        return ulidar_ext, fext


def fit_kaimal(f, S, u_ref, df, L=340.2):
    """fit kaimal spectrum assuming u_ref and L are known/fixed"""
    # filter out any zero S
    f = f[S > 0]
    S = S[S > 0]
    # perform fit
    logS = np.log10(S)
    def err(sig):
        Sk = sig**2 * 4*L/u_ref * df / (1 + 6*f*L/u_ref)**(5/3)
        return sum((np.log10(Sk) - logS)**2)
    x0 = [2]
    res = minimize(err, x0, bounds=[(0.05, np.inf)])
    if res.status:
        print('!!!Failed to fit kaimal!!! Minimize error: ' + res.message)
    return res.x


def fit_power_law(data, kw, lidar_meas=True):
    """Fit a power law profile where z_ref is z_hub"""
    nbeams = kw['beam_locs'].shape[1]
    zs = -kw['beam_locs'][2, :]
    if lidar_meas:  # if lidar meas passed in
        us = [np.mean(data[data[:, 2] == ibeam, 1]) for ibeam in range(1, nbeams+1)]
    else:  # assume [nt x nbeam]
        us = np.mean(data, axis=0)
    sse = lambda x: sum(us - x[0] * (zs / kw['z_hub']) ** x[1])**2  # uref, alpha
    res = minimize(sse, [np.mean(us), 0.2])
    if res.status:
        print('!!!Failed to fit power law!!! Minimize error: ' + res.message)
    return res.x


def fit_straight_psd(f, S):
    """Fit a straight line to a psd"""
    logf = np.log10(f)
    logS = np.log10(S)
    A = np.vstack((logf, np.ones(logf.size))).T
    x = np.linalg.lstsq(A, logS, rcond=None)[0]
    return x


def get_beam(lidar_meas, ibeam, filter_nans=True):
    """Extract the measurements corresponding to a lidar beam.
    Beam indexing starts at 1!
    """
    t_beam, u_beam = lidar_meas[lidar_meas[:, 2] == ibeam][:, :2].T
    if not t_beam.size:
        print(f'WARNING!!! Beam {ibeam} not found in lidar measurements.')
    if filter_nans:
        mask = ~np.isnan(u_beam)
        t_beam = t_beam[mask]
        u_beam = u_beam[mask]
    return t_beam, u_beam


def is_even(t):
    """Determine whether the time vector is even steps"""
    dt = t[1:] - t[:-1]
    return np.allclose(dt, dt[0])


def kaimal_psd(f, sig, L, u_ref, df):
    """theoretical discrete Kaimal psd"""
    return sig**2 * 4*L/u_ref * df / (1 + 6*f*L/u_ref)**(5/3)


def load_lidar_meas(lidar_path, twrap=True, hc_scale=True):
    """Load a lidar measurement file, optionall wrapping time and scaling by half-cone"""
    # load the lidar file
    lidar_meas = np.load(lidar_path)

    if twrap:
        # wrap last 100 s to first part of simulation (threw away first 100 s)
        lidar_meas = wrap_time(lidar_meas, 0)

    if hc_scale:
        # scale raw lidar by half-cone angle
        lidar_meas[:, 1] /= np.cos(half_cone)

    return lidar_meas


def load_hawc2_res(name, fname):
    """e.g., (dtu_10mw, dtu_10mw_wsp11.4_s1001)"""
    d = kw_dtu10mw if '10mw' in name else kw_nrel5mw
    hawc2_path = d['hawc2_dir'] + 'res/' + fname
    _ = ReadHawc2(hawc2_path)
    return _.ReadGtsdf()
    


def make_turb_htc(orig_htc, new_htc, tower_shadow=3, **kw):
    """Base htc file to a 700-s Mann turbulence file w/o power law and scaling
    
    Required keyword arguments: wsp, tint, tb_wid, tb_ht
    """
    nx = kw['nx']
    ny, nz = kw['ny'], kw['nz']
    tstart, tstop = 100 - 0.01, 700 - 0.01
    prefix = kw['prefix']
    htc = HTCFile(orig_htc)
    U, tint = kw['wsp'], kw['tint']
    wid, ht = kw['tb_wid'], kw['tb_ht']
    # set simulation time and file names
    htc.simulation.time_stop = 700
    htc.simulation.logfile = './log/' + prefix + '.log'
    htc.output.filename = './res/' + prefix
    # add the step wind
    htc.wind.wsp = U  # mean wind speed
    htc.wind.tint = tint  # turbulence intensity
    htc.wind.shear_format = [0, 0]  # no shear
    htc.wind.turb_format = 1  # mann
    htc.wind.tower_shadow_method = tower_shadow
    if 'mann' in htc.wind:
        del htc.wind.mann
    mann = htc.wind.add_section('mann')
    mann.add_line('filename_u', [f'./turb/{prefix}u.bin'])
    mann.add_line('filename_v', [f'./turb/{prefix}v.bin'])
    mann.add_line('filename_w', [f'./turb/{prefix}w.bin'])
    mann.add_line('box_dim_u', [nx, U * 600 / nx])  # repeat every 600 seconds
    mann.add_line('box_dim_v', [ny, wid / (ny - 1)])
    mann.add_line('box_dim_w', [nz, ht / (nz - 1)])
    mann.add_line('dont_scale', [1])
    # update the simulation time
    htc.output.time = [tstart, tstop]
    # save the new file
    htc.save(new_htc)


def sample_cw_lidar(data, idcs_los, t_scan, first_beam=1, blockage=False, **kw):
    """Take a HAWC2 dataset and sample it as if a CW lidar was sampling each beam
    (ignoring blade blockage).
    Args:
        data. 2D array of HAWC2 simulation.
        idcs_los. List of indices of line-of-sight measurements.
        t_scan. Time to finish a scan of all beams.
        first_beam. Which beam is the first sampled (1, 2, 3, etc.). Optional, default=1.
        blockage. Boolean to toggle blade blockage. If True, keyword arguments must
            include... Default=False.
    Returns:
        lidar_meas. 2D array of lidar sample time, line-of-sight measurements, and beam number.
    """
    nbeams = len(idcs_los)

    # get high-frequency time and line of sight data
    t_hf = data[:, 0]
    los_hf = data[:, idcs_los]

    # time vector for lidar samples and which beam is sampled at each step
    t_lidar = np.arange(t_hf[0], t_hf[-1] + 1e-6, t_scan/nbeams)
    beam_idx = (np.arange(t_lidar.size) + first_beam - 1) % nbeams

    # interpolate hf measurements to lidar scan times
    los_beams = np.empty((t_lidar.size, nbeams))
    for ibeam in range(nbeams):
        los_beams[:, ibeam] = np.interp(t_lidar, t_hf, los_hf[:, ibeam])

    # use matrix indexing to pull out correct beam measurement for each time step
    los_lidar = los_beams[range(t_lidar.size), beam_idx]

    # construct output array
    lidar_meas = np.vstack((t_lidar, los_lidar, beam_idx + 1)).T

    # if blockage, remove blocked points
    if blockage:
        # get the half-chord blockage angle (azimuthal angle corresponding to half of chord)
        idx_shft = kw['idx_shft']  # azimuth of shaft (blade 1)
        r_lidar = kw['r_lidar']  # radial distance from rotor center to lidar
        c_rlidar = kw['c_rlidar']  # chord length at r_lidar
        ang_halfchord = np.arcsin(0.5*c_rlidar / r_lidar) * 180 / np.pi  # angle that half the chord blocks [deg]
        # interpolate blade1 azimuth to lidar sample times
        azim1 = np.interp(t_lidar, t_hf, data[:, idx_shft])
        # create mask for when blades are blocking data
        azims = np.vstack([(azim1 + 120*i) % 360 for i in range(3)]).T  # 2d array of 3 blade azimuths
        blocked = np.any((azims <= ang_halfchord) | (azims >= 360 - ang_halfchord), axis=1)
        # mask the vlos data, leave time and beam number untouched
        lidar_meas[blocked, 1] = np.nan

    return lidar_meas


def save_timecon(re_con_path, u_lidar, beam_locs):
    """Save a PyConTurb-style constraints file. u_lidar is [nt x nbeams+1]."""
    t = u_lidar[:, 0]
    nbeams = beam_locs.shape[1]
    index = ['k', 'x', 'y', 'z'] + t.tolist()
    cols = [f'u_p{ibeam}' for ibeam in range(1, nbeams + 1)]
    data = np.vstack((np.zeros(nbeams),  # longitudinal, u = 0
                      np.zeros(nbeams),  # xP: assume located at rotor
                      -beam_locs[0, :],  # yP: equal to -x_global
                      -beam_locs[2, :],  # zP: equal to -z_global
                      u_lidar[:, 1:]))
    timecon = pd.DataFrame(data,
                           index=index, columns=cols)
    timecon.to_csv(re_con_path)
    return timecon


def shift_time(t, x, t0=0):
    """Shift a cyclic time series x in time"""
    # time to shift signal
    dt_shift = -t[0] + t0
    # new time vector
    t_shift = t + dt_shift
    # Fourier transform of signal
    dt = (t[-1] - t[0]) / (t.size - 1)
    X = np.fft.rfft(x) / x.size
    f = np.fft.rfftfreq(x.size, dt)
    # phases to shift each component by
    pha = (f * 2 * np.pi) * dt_shift  # in radians
    # new Fourier transform
    X_shift = X * np.exp(1j * pha)
    x_shift = np.fft.irfft(X_shift) * x.size
    return t_shift, x_shift


def wrap_time(data, wrap_idx, T=600):
    """Wrap a signal based on a time length"""
    data[:, wrap_idx] = data[:, wrap_idx] % T  # wrap to 600 s
    idx = np.argsort(data[:, wrap_idx])
    data = data[idx]
    return data