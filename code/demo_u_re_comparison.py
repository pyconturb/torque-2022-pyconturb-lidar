# -*- coding: utf-8 -*-
"""Compare raw lidar with u_re (raw resampled to even)
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from wetb.hawc2.Hawc2io import ReadHawc2
from _pct22utils import (wrap_time, adjust_hf, fit_power_law, fit_kaimal, kaimal_psd, 
                         extend_spectrum, get_beam, is_even, shift_time)
from _pct22vals import half_cone, nbeams, fc, beam_locs, z_hub, idcs_freewind



lidar_path = '../lidar/dtu_10mw_turb_unblocked.npy'  # path to lidar file

# ---------- pre-processing ----------

# load the lidar file
lidar_meas = np.load(lidar_path)

# wrap last 100 s to first part of simulation (threw away first 100 s)
lidar_meas = wrap_time(lidar_meas, 0)

# scale raw lidar by half-cone angle
lidar_meas[:, 1] /= np.cos(half_cone)

# whether signal is blocked or not
blocked = True if np.any(np.isnan(lidar_meas)) else False

# ---------- resample each beam to the same scanning time series ----------

tscan = lidar_meas[:, 0][::nbeams]
ntscan = tscan.shape[0]  # number of scan points

u_re = np.empty((ntscan, nbeams + 1))  # raw, resampled to even time steps
u_re[:, 0] = tscan

fig, axs = plt.subplots(2, 2, num=1, clear=True, figsize=(8, 5))

# turn raw lidar to even time steps, sampled on tscan
for ibeam in range(1, nbeams+1):
    # get time and u_beam (filtering nans)
    t_beam, u_beam = get_beam(lidar_meas, ibeam)

    # make even time step using least squares
    if not is_even(t_beam):
        raise ValueError('Least-squares fourier trasnform not coded yet!')
    else:
        t_even, u_even = t_beam, u_beam

    # adjust the start time of each beam using a time shift
    t_shift, u_shift = shift_time(t_even, u_even)
    np.testing.assert_allclose(tscan, t_shift)
    u_re[:, ibeam] = u_shift
    
    # plot things
    ax = axs[(ibeam-1) // 2, (ibeam-1) % 2]
    ax.plot(t_beam, u_beam, '-o', mfc='w', label='Lidar measurements')
    ax.plot(t_even, u_even, '-o', mfc='w', label='Even time step')
    ax.plot(t_shift, u_shift, '-o', mfc='w', label='Time-shifted')
    ax.set_xlim([0, 10])
    ax.set_ylim([12, 22])
    ax.grid()
    if ibeam == 1:
        ax.legend()
    ax.set_title(f'Beam {ibeam}')
    
fig.tight_layout()
