# -*- coding: utf-8 -*-
"""Demonstrate the high-frequency scaling and frequency extension.

Fits power law to data for u_ref, assumes L is 340.2.
"""
import matplotlib.pyplot as plt
import numpy as np
from wetb.hawc2.Hawc2io import ReadHawc2
from _pct22utils import (wrap_time, adjust_hf, fit_power_law, kaimal_psd, extend_spectrum, 
                         calculate_psd)
from _pct22vals import half_cone, nbeams, fc, kw_dtu10mw


# dtu 10 mw values
d = kw_dtu10mw
lidar_path = '../lidar/dtu_10mw_s1001_unblocked.npy'  # lidar file
hawc2_path = ('C:/Users/rink/git/G-pyconturb/torque-2022-pyconturb-lidar' + 
              '/dtu_10mw/res/dtu_10mw_s1001.hdf5')  # for free wind comparison

# load the HAWC2 file
read_h2 = ReadHawc2(hawc2_path)
data = read_h2.ReadGtsdf()

# wrap data, get free wind
idcs_freewind = [d['idx_wsp1'] + i*3 for i in range(nbeams)]  # horizontal wind speeds
data = wrap_time(data, 0)
t_free, u_free = data[:, 0], data[:, idcs_freewind]

# load the lidar file
lidar_meas = np.load(lidar_path)

# wrap last 100 s to first part of simulation
lidar_meas = wrap_time(lidar_meas, 0)

# scale by half-cone angle
lidar_meas[:, 1] /= np.cos(half_cone)

#%%

# fit mean profile (need u_ref for Kaimal fit)
u_ref, alpha = fit_power_law(lidar_meas, d)
print(f'\nu_ref: {u_ref:.3f}, alpha: {alpha:.3f}\n')

# desired time vector
t_ext = lidar_meas[:, 0]
dt = (t_ext[-1] - t_ext[0]) / (t_ext.size - 1)
T = t_ext[-1] - t_ext[0] + dt
df = 1 / T

fig, axs = plt.subplots(1, 2, num=5, clear=True, figsize=(11, 5))
    
# loop through each beam
for ibeam in range(1, nbeams+1):

    # get PSD of free wind for that beam
    u_free_beam = u_free[:, ibeam-1]
    f_free, S_free = calculate_psd(t_free, u_free_beam)

    # load the lidar measurement
    t_lidar, u_lidar = lidar_meas[lidar_meas[:, 2] == ibeam][:, :2].T
    
    print(f'Variance change to lidar: {np.var(u_lidar)/np.var(u_free_beam):.4f}')

    # make high-frequency adjustment for lidar averaging
    ulidar_hf, flidar, Slidar, Slidar_hf = adjust_hf(t_lidar, u_lidar, fc, return_S=True)
    
    print(f'Variance change w/hf: {np.var(ulidar_hf)/np.var(u_lidar):.4f}')
    
    # make the frequency extension to recover from sampling time
    ulidar_ext, fext, Sext, Xext, sig = extend_spectrum(t_lidar, ulidar_hf, flidar, Slidar_hf,
                                                        u_ref, t_ext, return_S=True, scale_var=True,
                                                        seed=None)
    
    print(f'Variance change w/ext: {np.var(ulidar_ext)/np.var(ulidar_hf):.4f}\n')
    
    # compare psd values
    L = 340.2
    Stheo = kaimal_psd(flidar, sig, L, u_ref, df)

    # make plot for beam 1
    if ibeam == 3:
        ax = axs[0]
        ax.plot(t_free, u_free_beam, label='Free wind')
        ax.plot(t_lidar, u_lidar, 'o', alpha=0.8, label='Lidar - raw')
        ax.plot(t_lidar, ulidar_hf, alpha=0.8, label='Lidar - hf')
        ax.plot(t_ext, ulidar_ext,  alpha=0.8, label='Lidar - hf+ext')
        ax.set_xlim([0, 30])
        ax.set_xlabel('Time [s]')
        ax.set_ylabel('Wind velocity [m/s]')
        ax.legend()
        
        ax = axs[1]
        ax.loglog(f_free[1:], S_free[1:], label='Free wind')
        ax.loglog(flidar[1:], Slidar_hf[1:], label='Lidar - hf')
        ax.loglog(fext[1:], Sext[1:], alpha=0.7, label='Lidar - hf+ext')
        ax.loglog(flidar[1:], Stheo[1:], label='Kaimal')
        ax.set_xlabel('Frequency [Hz]')
        ax.set_ylabel('PSD [(m/s)$^2$/Hz]')
        ax.legend()

fig.tight_layout()
plt.show()

