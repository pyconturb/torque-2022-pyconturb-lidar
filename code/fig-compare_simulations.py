# -*- coding: utf-8 -*-
"""Make plot to compare things
"""
import matplotlib.pyplot as plt
import numpy as np
from wetb.hawc2.Hawc2io import ReadHawc2
from _pct22utils import mask_data, load_hawc2_res
from _pct22vals import kw_dtu10mw, kw_nrel5mw, lidar_upwind, nbeams, seed_orig


# name, d = 'dtu_10mw', kw_dtu10mw
# name, d = 'nrel_5mw', kw_nrel5mw

save_fig = False

seed_ext = 7971  # for extending
seed_con = 1337  # for reconstructed turbulence
ibeam = 4  # which beam for first image

# wsp, tlim, ifig = 11.4, [300, 700], 4
wsp, tlim, ifig = 24, [300, 700], 5
tshift = lidar_upwind / wsp  # how much to shift the constrained turbulence (lidar upwind over wsp)


ishift = int(tshift / 0.01)
fig, axs = plt.subplots(2, 4, num=ifig, figsize=(15, 7.5), clear=True)

for name in ['dtu_10mw', 'nrel_5mw']:
    print('\n', name, '\n')
    d = kw_dtu10mw if 'dtu' in name else kw_nrel5mw

    fname_orig =  f'{name}_wsp{wsp}_s{seed_orig}.hdf5'
    fname_unc =  f'{name}_wsp{wsp}_s{seed_orig+1}.hdf5'
    fname_con = f'{name}_wsp{wsp}_s{seed_orig}_{seed_ext}_ext_cs{seed_con}.hdf5'
    
    idcs_los = [d['idx_lidar1'] + i*5 for i in range(nbeams)]  # lines of sight
    idcs_freewind = [d['idx_wsp1'] + i*3 for i in range(nbeams)]  # horizontal wind speeds
    idc_los = idcs_los[ibeam - 1]  # line of sight index
    idc_freewind = idcs_freewind[ibeam - 1]  # free wind index
    
    # load the original simulation
    data_orig = load_hawc2_res(name, fname_orig)
    t_orig = data_orig[:, 0]
    
    # load the unconstrained simulation
    data_unc = load_hawc2_res(name, fname_unc)
    t_unc = data_unc[:, 0]
    
    # load the constrained simulation
    data_con = load_hawc2_res(name, fname_con)
    t_con = data_con[:, 0]
    
    
    # plt.rcParams['font.size'] = 12
    
    
    ax_x = 0 if 'dtu' in name else 2
    
    # ----- axes 0, 0: free wind near beam 1 -----
    x_orig = data_orig[:, idc_freewind]
    los = data_orig[:, idc_los]
    x_con = data_con[:, idc_freewind]
    x_unc = data_unc[:, idc_freewind]
    ax = axs[0, 0 + ax_x]
    ax.plot(*mask_data(t_orig, x_orig, tlim), label='Original')
    ax.plot(*mask_data(t_con + tshift, x_con, tlim), label='Constrained')
    ax.plot(*mask_data(t_unc, x_unc, tlim), c='0.7', zorder=-1, label='Unconstrained')
    # ax.plot(t_orig, los, '0.7', label='Line of sight')
    ax.set_xlim(tlim)
    ax.set_title(f'u(t) at Point {ibeam} [m/s]')
    if 'dtu' in name:
        ax.legend(fontsize='small')
    

    plot_info = [('idx_om', 'Rotor speed [rad/s]'),
                 ('idx_raws', 'Rotor-avg wind speed [m/s]'),
                 ('idx_pit', 'Blade pitch [deg]')]
    for iax, (key, title) in enumerate(plot_info):
        x_orig = data_orig[:, d[key]]
        x_con = data_con[:, d[key]]
        x_unc = data_unc[:, d[key]]
        ax = axs[(iax+1) // 2, (iax+1)%2 + ax_x]
        ax.plot(*mask_data(t_orig, x_orig, tlim), label='Original')
        ax.plot(*mask_data(t_con + tshift, x_con, tlim), label='Constrained')
        ax.plot(*mask_data(t_unc, x_unc, tlim), c='0.7', zorder=-1, label='Unconstrained')
        ax.set(xlim=tlim, title=title,
               xlabel='Time [s]' if iax else None)
        #       f'MAE: {mae_unc:.3f} & {mae_con:.3f} ({perc:.1f}%) ')



fig.tight_layout(rect=[0, 0, 1, 0.95])
fig.text(0.25, 0.95, 'DTU 10 MW', ha='center', fontsize='x-large')
fig.text(0.75, 0.95, 'NREL 5 MW', ha='center', fontsize='x-large')

if save_fig:
    fig_dir = 'C:/Users/rink/Documents/publications/2021-08-21_torque_pyconturb/abstract/figures/'
    plt.savefig(fig_dir + 'compare_simulations.png', dpi=150)
