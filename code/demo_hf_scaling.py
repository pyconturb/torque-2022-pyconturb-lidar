# -*- coding: utf-8 -*-
"""Load a lidar file and preprocess it to get PyConTurb-style constraints
"""
import matplotlib.pyplot as plt
import numpy as np
from wetb.hawc2.Hawc2io import ReadHawc2
from _pct22utils import calculate_psd, wrap_time, fit_straight_psd, adjust_hf
from _pct22vals import half_cone, idcs_freewind, fc


data_dir = '../lidar/'
fname = 'dtu_10mw_turb'
hawc2_path = 'C:/Users/rink/git/G-pyconturb/torque-2022-pyconturb-lidar/dtu_10mw/res/dtu_10mw_turb.hdf5'

# load the HAWC2 file
read_h2 = ReadHawc2(hawc2_path)
data = read_h2.ReadGtsdf()

# wrap data, get free wind
data = wrap_time(data, 0)
t_free, u_free = data[:, 0], data[:, idcs_freewind]

# unblocked
lidar_path = data_dir + fname + '_unblocked.npy'
lidar_meas = np.load(lidar_path)

# wrap time
lidar_meas = wrap_time(lidar_meas, 0)

# scale by the half-cone offset
lidar_meas[:, 1] /= np.cos(half_cone)

#%%

# demo with beam 1
ibeam = 1

# get lidar and free-wind psd
t_lidar, u_lidar = lidar_meas[lidar_meas[:, 2] == ibeam][:, :2].T
ffree, Sfree = calculate_psd(t_free, u_free[:, ibeam-1])

# perform hf correction
ulidar_sc, flidar, Slidar, Slidar_sc = adjust_hf(t_lidar, u_lidar, fc,
                                                 scale_var=True, return_S=True)

# get fits with and without scaling
fhf = flidar[flidar > fc]
x = fit_straight_psd(fhf, Slidar[flidar > fc])
xsc = fit_straight_psd(fhf, Slidar_sc[flidar > fc])

print(f'Variance change: {np.var(ulidar_sc)/np.var(u_lidar):.4f}')

fig, axs = plt.subplots(2, 2, num=4, clear=True, figsize=(9, 5))

ax = axs[0, 0]
ax.plot(t_free, u_free[:, ibeam-1])
ax.plot(t_lidar, u_lidar, alpha=0.7)
ax.set_xlim([0, 300])
ax.set_ylabel('Wind speed [m/s]')

ax = axs[0, 1]
ax.loglog(ffree, Sfree)
ax.loglog(flidar, Slidar, alpha=0.8)
ax.loglog(fhf, 10**x[1] * fhf**x[0], 'r', alpha=0.7)
ax.text(0.7, 0.95, f'$\\alpha$ = {x[0]:.4f}', transform=ax.transAxes, ha='left', va='top')
ax.set_ylabel('PSD [(m/s)$^2$/Hz]')

ax = axs[1, 0]
ax.plot(t_free, u_free[:, ibeam-1])
ax.plot(t_lidar, ulidar_sc, alpha=0.7)
ax.set_xlim([0, 300])
ax.set_ylabel('Wind speed [m/s]')
ax.set_xlabel('Time [s]')

ax = axs[1, 1]
ax.loglog(ffree, Sfree)
ax.loglog(flidar, Slidar_sc, alpha=0.8)
ax.loglog(fhf, 10**xsc[1] * fhf**xsc[0], 'r', alpha=0.7)
ax.text(0.7, 0.95, f'$\\alpha$ = {xsc[0]:.4f}', transform=ax.transAxes, ha='left', va='top')
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('PSD [(m/s)$^2$/Hz]')

plt.tight_layout()
