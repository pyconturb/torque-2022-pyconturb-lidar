# -*- coding: utf-8 -*-
"""
"""
import matplotlib.pyplot as plt
import numpy as np


zR = 6.5  # rayleigh length
half_width = 6  # half-width of the integration value [multiples of zR]
F = 100  # focus length along the beam
no_int = 100  # number of integration points

dr = np.linspace(-50, 50, 201)  # r - F
wc = 1/np.pi * zR / (zR**2 + dr**2)

dr_int = np.linspace(-half_width*zR, half_width*zR, no_int)
wc_int = 1/np.pi * zR / (zR**2 + dr_int**2)

fig, ax = plt.subplots(1, num=12, clear=True, figsize=(8, 4))
plt.plot(dr, wc)
plt.plot(dr_int, wc_int, 'o', mfc='none', mec='r', alpha=0.5)
ylim = ax.get_ylim()
plt.fill_between([-half_width*zR, half_width*zR], ylim[1], zorder=-1, facecolor='0.4', alpha=0.2)
ax.set_xlim([dr[0], dr[-1]])
ax.set_ylim([0, ylim[1]])
plt.xlabel('r - F [m]')
plt.ylabel('Weighting function [-]')
plt.tight_layout()
