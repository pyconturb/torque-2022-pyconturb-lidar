; NREL_5MW_reference_wind_turbine
;
  begin simulation;
    time_stop	700;
    solvertype	1;	(newmark)
    on_no_convergence	continue;
    logfile	./log/nrel_5mw_wsp11.4_s1003.log;	
;
    begin newmark;
      deltat	0.01;
    end newmark;
  end simulation;
;
  begin new_htc_structure;
;  beam_output_file_name  ./log/NREL_5MW_reference_wind_turbine_beam.dat;                    Optional - Calculated beam properties of the bodies are written to file
;  body_output_file_name  ./log/NREL_5MW_reference_wind_turbine_body.dat;                    Optional - Body initial position and orientation are written to file
;  body_eigenanalysis_file_name ./eigenfrq/NREL_5MW_reference_wind_turbine_body_eigen.dat;
;  structure_eigenanalysis_file_name ./eigenfrq/NREL_5MW_reference_wind_turbine_strc_eigen.dat ;
;-------------------------------------------------------------------------------------------------------------------------------
    begin main_body;	tower 87m
      name	tower;
      type	timoschenko;
      nbodies	1;
      node_distribution	c2_def;
      damping_posdef	0 0 0 0.00362 0.00362 0.0033;	Mx My Mz Kx Ky Kz , M´s raises overall level, K´s raises high freguency level
      begin timoschenko_input;
        filename	./data/NREL_5MW_st.txt;
        set	1 1;
      end timoschenko_input;
      begin c2_def;	Definition of centerline (main_body coordinates)
        nsec	8;
        sec	1 0 0 0 0;	x,y,z,twist
        sec	2 0 0 -10 0;
        sec	3 0 0 -20 0;
        sec	4 0 0 -30 0;
        sec	5 0 0 -45 0;
        sec	6 0 0 -60 0;
        sec	7 0 0 -70 0;
        sec	8 0 0 -87.6 0;
      end c2_def;
    end main_body;
;
    begin main_body;
      name	towertop;
      type	timoschenko;
      nbodies	1;
      node_distribution	c2_def;
      damping_posdef	0 0 0 3e-05 3e-05 0.0002;
      concentrated_mass	2 0 1.9 0.21256 240000 1741490 170000 1741490;	Nacelle mass and inertia
      begin timoschenko_input;
        filename	./data/NREL_5MW_st.txt;
        set	2 1;
      end timoschenko_input;
      begin c2_def;	Definition of centerline (main_body coordinates)
        nsec	2;
        sec	1 0 0 0 0;	x,y,z,twist
        sec	2 0 0 -1.96256 0;
      end c2_def;
    end main_body;
;
    begin main_body;
      name	shaft;
      type	timoschenko;
      nbodies	1;
      node_distribution	c2_def;
      damping_posdef	0 0 0 0.000465 0.000465 0.0070725;	"tuned by Anyd 23/5/13 to 31.45 log decr. damping for free free with stiff rotor and tower"
      concentrated_mass	1 0 0 0 0 0 0 5025497.444;	generator equivalent slow shaft
      concentrated_mass	5 0 0 0 56780 0 0 115926;	hub mass and inertia;
      begin timoschenko_input;
        filename	./data/NREL_5MW_st.txt;
        set	3 1;
      end timoschenko_input;
      begin c2_def;	Definition of centerline (main_body coordinates)
        nsec	5;
        sec	1 0 0 0 0;	Tower top x,y,z,twist
        sec	2 0 0 1 0;
        sec	3 0 0 2 0;
        sec	4 0 0 3.1071 0;	Main bearing
        sec	5 0 0 5.0191 0;	Rotor centre
      end c2_def;
    end main_body;
;
    begin main_body;
      name	hub1;
      type	timoschenko;
      nbodies	1;
      node_distribution	c2_def;
      damping_posdef	0 0 0 3e-06 3e-06 2e-05;
      begin timoschenko_input;
        filename	./data/NREL_5MW_st.txt;
        set	4 1;
      end timoschenko_input;
      begin c2_def;	Definition of centerline (main_body coordinates)
        nsec	2;
        sec	1 0 0 0 0;	x,y,z,twist
        sec	2 0 0 1.5 0;
      end c2_def;
    end main_body;
;
    begin main_body;
      name	hub2;
      copy_main_body	hub1;
    end main_body;
;
    begin main_body;
      name	hub3;
      copy_main_body	hub1;
    end main_body;
;
    begin main_body;
      name	blade1;
      type	timoschenko;
      nbodies	9;
      node_distribution	c2_def;
      damping_posdef	0 0 0 0.00141 0.00239 4.5e-05;
      begin timoschenko_input;
        filename	./data/NREL_5MW_st.txt;
        set	5 1;	set subset
      end timoschenko_input;
      begin c2_def;	Definition of centerline (main_body coordinates)
        nsec	19;
        sec	1 0 0 0 0;	x.y.z. twist
        sec	2 -0.0027 0.0006 1.367 -13.308;
        sec	3 -0.1057 0.025 4.1 -13.308;
        sec	4 -0.2501 0.0592 6.833 -13.308;
        sec	5 -0.4592 0.1086 10.25 -13.308;
        sec	6 -0.5699 0.1157 14.35 -11.48;
        sec	7 -0.5485 0.0983 18.45 -10.162;
        sec	8 -0.5246 0.0832 22.55 -9.011;
        sec	9 -0.4962 0.0679 26.65 -7.795;
        sec	10 -0.4654 0.0534 30.75 -6.544;	50% blade radius
        sec	11 -0.4358 0.0409 34.85 -5.361;
        sec	12 -0.4059 0.0297 38.95 -4.188;
        sec	13 -0.3757 0.0205 43.05 -3.125;
        sec	14 -0.3452 0.014 47.15 -2.319;
        sec	15 -0.3146 0.0084 51.25 -1.526;
        sec	16 -0.2891 0.0044 54.667 -0.863;
        sec	17 -0.2607 0.0017 57.4 -0.37;
        sec	18 -0.1774 0.0003 60.133 -0.106;
        sec	19 -0.1201 0 61.5 0;
      end c2_def;
    end main_body;
;
    begin main_body;
      name	blade2;
      copy_main_body	blade1;
    end main_body;
;
    begin main_body;
      name	blade3;
      copy_main_body	blade1;
    end main_body;
;-------------------------------------------------------------------------------------------------------------------------------
;
    begin orientation;
      begin base;
        body	tower;
        inipos	0 0 0;	initial position of node 1
        body_eulerang	0 0 0;
      end base;
;
      begin relative;
        body1	tower last;
        body2	towertop 1;
        body2_eulerang	0 0 0;
      end relative;
;
      begin relative;
        body1	towertop last;
        body2	shaft 1;
        body2_eulerang	90 0 0;
        body2_eulerang	5 0 0;	5 deg tilt angle
        body2_ini_rotvec_d1	0 0 -1 0.5;	body initial rotation velocity x.y.z.angle velocity[rad/s]  (body 2 coordinates)
;      body2_ini_rotvec_d1 0.0 0.0 -1.0 0.9424 ; body initial rotation velocity x.y.z.angle velocity[rad/s]  (body 2 coordinates)
      end relative;
;
      begin relative;
        body1	shaft last;
        body2	hub1 1;
        body2_eulerang	-90 0 0;
        body2_eulerang	0 180 0;
        body2_eulerang	2.5 0 0;	2.5deg cone angle
      end relative;
;
      begin relative;
        body1	shaft last;
        body2	hub2 1;
        body2_eulerang	-90 0 0;
        body2_eulerang	0 60 0;
        body2_eulerang	2.5 0 0;	2.5deg cone angle
      end relative;
;
      begin relative;
        body1	shaft last;
        body2	hub3 1;
        body2_eulerang	-90 0 0;
        body2_eulerang	0 -60 0;
        body2_eulerang	2.5 0 0;	2.5deg cone angle
      end relative;
;
      begin relative;
        body1	hub1 last;
        body2	blade1 1;
        body2_eulerang	0 0 0;
      end relative;
;
      begin relative;
        body1	hub2 last;
        body2	blade2 1;
        body2_eulerang	0 0 0;
      end relative;
;
      begin relative;
        body1	hub3 last;
        body2	blade3 1;
        body2_eulerang	0 0 0;
      end relative;
;
    end orientation;
;-------------------------------------------------------------------------------------------------------------------------------
    begin constraint;
;
      begin fix0;	fixed to ground in translation and rotation of node 1
        body	tower;
      end fix0;
;
      begin fix1;
        body1	tower last;
        body2	towertop 1;
      end fix1;
;
      begin bearing1;	free bearing
        name	shaft_rot;
        body1	towertop last;
        body2	shaft 1;
        bearing_vector	2 0 0 -1;	x=coo (0=global.1=body1.2=body2) vector in body2 coordinates where the free rotation is present
      end bearing1;
;
      begin fix1;
        body1	shaft last;
        body2	hub1 1;
      end fix1;
;
      begin fix1;
        body1	shaft last;
        body2	hub2 1;
      end fix1;
;
      begin fix1;
        body1	shaft last;
        body2	hub3 1;
      end fix1;
;
      begin bearing2;
        name	pitch1;
        body1	hub1 last;
        body2	blade1 1;
        bearing_vector	2 0 0 -1;
      end bearing2;
;
      begin bearing2;
        name	pitch2;
        body1	hub2 last;
        body2	blade2 1;
        bearing_vector	2 0 0 -1;
      end bearing2;
;
      begin bearing2;
        name	pitch3;
        body1	hub3 last;
        body2	blade3 1;
        bearing_vector	2 0 0 -1;
      end bearing2;
    end constraint;
;
  end new_htc_structure;
;----------------------------------------------------------------------------------------------------------------------------------------------------------------
  begin wind;
    density	1.225;	to be checked
    wsp	11.4;
    tint	0.1985964912280702;
    horizontal_input	1;	0=false, 1=true
    windfield_rotations	0 0 0;	yaw, tilt, rotation
    center_pos0	0 0 -90;
    shear_format	3 0.2;	0=none,1=constant,2=log,3=power,4=linear
    turb_format	1;	0=none, 1=mann,2=flex
    tower_shadow_method	3;	0=none, 1=potential flow, 2=jet
;
    begin tower_shadow_potential_2;
      tower_mbdy_link	tower;
      nsec	2;
      radius	0 3;
      radius	87.6 1.935;
    end tower_shadow_potential_2;
    scale_time_start	100;
    begin mann;
      create_turb_parameters	33.6 1.0 3.9 1003 1;
      filename_u	./turb/nrel_5mw_wsp11.4_s1003_turb_u.bin;
      filename_v	./turb/nrel_5mw_wsp11.4_s1003_turb_v.bin;
      filename_w	./turb/nrel_5mw_wsp11.4_s1003_turb_w.bin;
      box_dim_u	8192 0.8349609375;
      box_dim_v	32 4.193548387096774;
      box_dim_w	32 4.193548387096774;
    end mann;
  end wind;
;
  begin aero;
    nblades	3;
    hub_vec	shaft -3;	rotor rotation vector (normally shaft composant directed from pressure to sustion side)
    link	1 mbdy_c2_def blade1;
    link	2 mbdy_c2_def blade2;
    link	3 mbdy_c2_def blade3;
    ae_filename	./data/NREL_5MW_ae.txt;
    pc_filename	./data/NREL_5MW_pc.txt;
    induction_method	1;	0=none, 1=normal
    aerocalc_method	1;	0=ingen aerodynamic, 1=med aerodynamic
    aerosections	30;
    ae_sets	1 1 1;
    tiploss_method	1;	0=none, 1=prandtl
    dynstall_method	2;	0=none, 1=stig øye method,2=mhh method
  end aero;
;
  begin aerodrag;
    begin aerodrag_element;
      mbdy_name	tower;
      aerodrag_sections	uniform 10;
      nsec	2;
      sec	0 0.6 6;	tower bottom
      sec	87.6 0.6 3.87;	tower top
    end aerodrag_element;
;
    begin aerodrag_element;	Nacelle drag side
      mbdy_name	shaft;
      aerodrag_sections	uniform 2;
      nsec	2;
      sec	0 0.8 10;
      sec	5.02 0.8 10;
    end aerodrag_element;
  end aerodrag;
;
;-------------------------------------------------------------------------------------------------
  begin dll;
;
    begin type2_dll;	1. discon controller interface
      name	nrel_5mw_interface;
      filename	./control/nrel_5mw_interface.dll;
      dll_subroutine_init	initialize_type2dll;
      dll_subroutine_update	update_type2dll;
      arraysizes_init	100 1;
      arraysizes_update	100 100;	
;
      begin init;	parameters needed to initialize DLL
        constant	1 97;	Gear ratio [-]
        constant	2 3;	Number of blades [-]
        constant	3 0;	Enable DLL log [0=disable, 1=enable]
      end init;
;
      begin output;	HAWC2 channels passed to the interface DLL
        general status;	Status [-]
        general time;	Time [s]
        constraint bearing1	shaft_rot 1 only 2;	LSS [rad/s]
        constraint bearing2	pitch1 1 only 1;	Blade1 pitch angle [rad]
        constraint bearing2	pitch2 1 only 1;	Blade1 pitch angle [rad]
        constraint bearing2	pitch3 1 only 1;	Blade1 pitch angle [rad]
        wind free_wind_hor	1 0 0 -90 only 1;	Hub-height wind speed [m/s]
      end output;	
;
      begin actions;	actions applied to the turbine
        mbdy	moment_int shaft 1 3 shaft towertop 2;	Generator LSS torque [Nm]
        constraint	bearing2 angle pitch1;	Angle pitch1 bearing    [rad]
        constraint	bearing2 angle pitch2;	Angle pitch2 bearing    [rad]
        constraint	bearing2 angle pitch3;	Angle pitch3 bearing    [rad]
      end actions;
;
    end type2_dll;
;
    begin hawc_dll;	2. damper
      filename	./control/damper.dll;
      dll_subroutine	damp;
      arraysizes	15 15;
      begin output;
        general time;	1
        general constant	5;
        general constant	10;
        general constant	-10;
        mbdy state	vel towertop 1 1 tower;
      end output;	
;
      begin actions;
        mbdy	force_ext towertop 2 1 towertop;
      end actions;
    end hawc_dll;
;
    begin type2_dll;	3. gearbox converter
      name	gear;
      filename	./control/hss_convert.dll;
      arraysizes_init	3 1;
      arraysizes_update	2 2;
      begin init;
        constant	1 1;	number of used sensors - in this case only 1
        constant	2 1000;	unit conversion factor
      end init;
      begin output;
        mbdy momentvec	shaft 1 1 shaft # only 3;
      end output;	
;
      begin actions;
        mbdy	moment_ext towertop 2 3 shaft;
      end actions;
    end type2_dll;
;
  end dll;
;
;----------------------------------------------------------------------------------------------------------------------------------------------------------------
;
  begin output;
    filename	./res/nrel_5mw_wsp11.4_s1003;
    data_format	gtsdf;
    buffer	9999;	
;
    time	99.99 699.99;
    general time;
    constraint bearing1	shaft_rot 2;	angle and angle velocity
    constraint bearing2	pitch1 5;	angle and angle velocity
    constraint bearing2	pitch2 5;	angle and angle velocity
    constraint bearing2	pitch3 5;	angle and angle velocity
    aero omega;
    aero torque;
    aero power;
    aero thrust;
    wind free_wind	1 0 0 -90;	local wind at fixed position: coo (1=global,2=non-rotation rotor coo.), pos x, pos y, pos z
    aero wsp_rotor_avg	1 2;	rotor-averaged wind speed in Vy global
; Moment
    mbdy momentvec	tower 1 1 tower # tower base;
    mbdy momentvec	tower 7 2 tower # yaw bearing;
    mbdy momentvec	shaft 4 1 shaft # main bearing;
    mbdy momentvec	blade1 2 2 blade1 # blade 1 root;
    mbdy momentvec	blade2 2 2 blade2 # blade 2 root;
    mbdy momentvec	blade3 2 2 blade3 # blade 3 root;
    mbdy momentvec	blade1 10 1 local # blade 1 50% local e coo;
    mbdy momentvec	blade2 10 1 local # blade 2 50% local e coo;
    mbdy momentvec	blade3 10 1 local # blade 3 50% local e coo;	
; Displacements and accellerations
    mbdy state	pos tower 7 1 global only 1 # Tower top FA displ;
    mbdy state	pos tower 7 1 global only 2 # Tower top SS displ;
    mbdy state	acc tower 7 1 global only 1 # Tower top FA acc;
    mbdy state	acc tower 7 1 global only 2 # Tower top SS acc;	
;
    mbdy state	pos blade1 18 1 blade1 # blade 1 tip pos;
    mbdy state	pos blade2 18 1 blade2 # blade 2 tip pos;
    mbdy state	pos blade3 18 1 blade3 # blade 3 tip pos;
    mbdy state	pos blade1 18 1 global # gl blade 1 tip pos;	
; - Monitor Aerodynamics - ;
    aero windspeed	3 1 1 63;	wind seen from the blade: coo(1=local ae,2=blade,3=global,4=rotor polar), blade number, component xyz, blade radius
    aero windspeed	3 1 2 63;
    aero windspeed	3 1 3 63;
    aero alfa	1 45;
    aero alfa	2 45;
    aero alfa	3 45;	
;
; control dll
    dll inpvec	1 1 # mgen lss;	[Nm]
    dll inpvec	1 2 # demanded blade1 pitch;	[rad]
    dll inpvec	1 3 # demanded blade2 pitch;	[rad]
    dll inpvec	1 4 # demanded blade3 pitch;	[rad]
    dll inpvec	1 5 # electrical power;	[kW]
    dll inpvec	1 6 # genspeed;	[rpm]
; Nacelle-mounted lidar and wsp at locations
    aero nacelle_lidar	0 0 0 15 45 100 6.5 6 100 1 # beam_1;
    aero nacelle_lidar	0 0 0 15 135 100 6.5 6 100 2 # beam_2;
    aero nacelle_lidar	0 0 0 15 225 100 6.5 6 100 3 # beam_3;
    aero nacelle_lidar	0 0 0 15 315 100 6.5 6 100 4 # beam_4;
    wind free_wind	1 18.3013 -101.5926 -108.3013 # wsp_beam1;
    wind free_wind	1 18.3013 -101.5926 -71.6987 # wsp_beam2;
    wind free_wind	1 -18.3013 -101.5926 -71.6987 # wsp_beam3;
    wind free_wind	1 -18.3013 -101.5926 -108.3013 # wsp_beam4;	
;
  end output;	
;
exit;